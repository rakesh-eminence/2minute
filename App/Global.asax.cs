﻿using App.Common;
using App.Models;
using BrainAppUtility;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace App
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Application["TotalApplication"] = 0;
            Application["totalSession"] = 0;

            Application["TotalApplication"] = (int)Application["TotalApplication"] + 1;


        }



        private void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            m1TestEntities db;
            List<Proc_GetAllUserForUnlockTrack_Result> ObjGetUserList = new List<Proc_GetAllUserForUnlockTrack_Result>();
            using (db = new m1TestEntities())
            {
                ObjGetUserList = db.Proc_GetAllUserForUnlockTrack().ToList();

            }
            using (db = new m1TestEntities())
            {
                PushNotifications ObjPushNotification = new PushNotifications();
                foreach (var item in ObjGetUserList)
                {

                    if (item.DeviceType == "Android")
                    {
                        var ObjUpdateTrack = db.Proc_UnlockTrackforUser(item.UserId, item.fkProgramId).FirstOrDefault();

                        if (!string.IsNullOrEmpty(ObjUpdateTrack.BasicTrackRelease))
                        {
                            ObjPushNotification.SendNotification_Android(item.DeviceToken, CommonMessage.BasicTrackRelease, "Track", item.fkProgramId);
                            db.proc_InsertNotification(CommonMessage.BasicTrackRelease, item.UserId, item.fkProgramId);
                        }
                        else if (!string.IsNullOrEmpty(ObjUpdateTrack.BonusTrackRelease))
                        {
                            ObjPushNotification.SendNotification_Android(item.DeviceToken, CommonMessage.BonusTrackRelease, "Bonus", item.fkProgramId);
                            db.proc_InsertNotification(CommonMessage.BonusTrackRelease, item.UserId, item.fkProgramId);
                        }

                    }
                    else if (item.DeviceType == "IOS")
                    {
                        var ObjUpdateTrack = db.Proc_UnlockTrackforUser(item.UserId, item.fkProgramId).FirstOrDefault();
                        if (!string.IsNullOrEmpty(ObjUpdateTrack.BasicTrackRelease))
                        {
                            ObjPushNotification.SendNotification_IOS(item.DeviceToken, CommonMessage.BasicTrackRelease, "Track", item.fkProgramId);
                            db.proc_InsertNotification(CommonMessage.BasicTrackRelease, item.UserId, item.fkProgramId);
                        }
                        else if (!string.IsNullOrEmpty(ObjUpdateTrack.BonusTrackRelease))
                        {
                            ObjPushNotification.SendNotification_IOS(item.DeviceToken, CommonMessage.BonusTrackRelease, "Bonus", item.fkProgramId);
                            db.proc_InsertNotification(CommonMessage.BonusTrackRelease, item.UserId, item.fkProgramId);
                        }
                    }

                }
            }

        }







    }
}
