﻿using App.Common;
using App.Models;
using Stripe;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;
using static App.viewmodel.StripeViewModel;

namespace App.Controllers
{
    public class PaymentStripeController : ApiController
    {
        // GET: api/PaymentStripe
        CommonHelper obj = new CommonHelper();
        private m1TestEntities db = new m1TestEntities();
        public HttpResponseMessage phonepayment (IosViewMdoel Model)
        {


            string publicKey = WebConfigurationManager.AppSettings["StripeSecretkey"];
            // Set your secret key: remember to change this to your live secret key in production
            // See your keys here: https://dashboard.stripe.com/account/apikeys
            StripeConfiguration.ApiKey = publicKey;
            try
            {
                StripeConfiguration.ApiKey = "sk_test_2Zicj9FG2HvZNhgcNKb71gHc00UQyCnkOm";

                var service = new PaymentIntentService();
                var options = new PaymentIntentCreateOptions
                {
                    Amount = Model.Amount,
                    Currency = Model.Currency,
                    PaymentMethodTypes = new List<string> { "card" },
                };
                PaymentIntent intent = service.Create(options);

                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, intent, Request);

            }
            catch (Exception ex)
            {

                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, "", Request);

            }

        }


        [HttpPost]
        public HttpResponseMessage StripePayment(IosViewMdoel Model)
        {

            string StripeSecretkey = ConfigurationManager.AppSettings["StripeSecretkey"].ToString();
            // Set your secret key: remember to change this to your live secret key in production
            // See your keys here: https://dashboard.stripe.com/account/apikeys
            StripeConfiguration.ApiKey = StripeSecretkey;
            try
            {
                var res = db.tbl_LoginMaster.Where(x => x.pkId == Model.UserId).FirstOrDefault();

                string PrivateKey = WebConfigurationManager.AppSettings["StripeSecretkey"];
                var customers = new CustomerService();
                var charges = new ChargeService();
                var customer = customers.Create(new CustomerCreateOptions
                {
                    Email = string.IsNullOrEmpty(res.EmailId) ? "test@gmail.com" : res.EmailId,
                    Source = Model.Token,
                    Name = string.IsNullOrEmpty(res.Name) ? "test" : res.Name,
                    Address = new AddressOptions { City = "test", Country = "US", PostalCode = "test", State = "test", Line1 = "test" },
                });

                var charge = charges.Create(new ChargeCreateOptions
                {
                    Amount = Model.Amount,
                    Description = "Meditation payment",
                    Currency = Model.Currency,
                    Customer = customer.Id

                });

                db.Proc_SaveStripePaymentDetail(charge.BalanceTransactionId, charge.CustomerId, charge.Amount, Model.UserId, charge.Paid, Model.ProgramId, charge.PaymentMethodId);
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, charge.BalanceTransactionId, Request);
            }
            catch (Exception ex)
            {
                string ex1 = ex.ToString();
                return obj.APIResponse(HttpStatusCode.BadRequest, true, "", "", Request);
            }
        }



        // GET: api/PaymentStripe/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/PaymentStripe
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/PaymentStripe/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/PaymentStripe/5
        public void Delete(int id)
        {
        }
    }
}

//cd C:\Windows\Microsoft.NET\Framework\v4.0.30319 
//InstallUtil.exe E:\BrainAPIRepo\MeditationService\bin\Debug\MeditationService.exe
//InstallUtil.exe C:\m2\MeditationService\MeditationService.exe

//sc delete Service1
