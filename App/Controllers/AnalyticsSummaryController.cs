﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace App.Controllers
{
    public class AnalyticsSummaryController : Controller
    {
        // GET: AnalyticsSummary
        public ActionResult Index()
        {
            return View();
        }

        // GET: AnalyticsSummary/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: AnalyticsSummary/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AnalyticsSummary/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: AnalyticsSummary/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: AnalyticsSummary/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: AnalyticsSummary/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: AnalyticsSummary/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
