﻿using App.Common;
using App.Models;
using App.viewmodel;
using BrainAppUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace App.Controllers
{
    public class NotificationUIController : Controller
    {
        private m1TestEntities db;
        // GET: NotificationUI
        public ActionResult Index()
        {
            return View();
        }

        // GET: NotificationUI/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: NotificationUI/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: NotificationUI/Create
        [HttpPost]
        public ActionResult Create(tbl_Notification collection)
        {
            try
            {
            
                // TODO: Add insert logic here
                using (db = new m1TestEntities())
                {
                    List<NotificationViewModel> ObjLoginViewModel = db.tbl_LoginMaster.Where(x => x.IsAdmin != true).Select(y => new NotificationViewModel()
                    {
                        UserId = y.pkId,
                        DeviceType = y.DeviceType,
                        DeviceToken = y.DeviceToken
                    }).ToList();//.OrderBy(x1=>x1.UserId).Skip(200).Take(100).ToList();
                    PushNotifications ObjPushNotification = new PushNotifications();
                    foreach (NotificationViewModel item in ObjLoginViewModel)
                    {
                        if (!string.IsNullOrEmpty(item.DeviceToken) && !string.IsNullOrEmpty(item.DeviceType))
                        {
                            if (item.DeviceType == "Android")
                            {

                                ObjPushNotification.SendNotification_Android(item.DeviceToken, collection.Text, "", -1);
                                db.proc_InsertNotification(CommonMessage.BonusTrackRelease, item.UserId, -1);
                            }
                            else if (item.DeviceType == "IOS")
                            {
                                ObjPushNotification.SendNotification_IOS(item.DeviceToken, collection.Text, "", -1);
                                db.proc_InsertNotification(CommonMessage.BonusTrackRelease, item.UserId, -1);

                            }
                        }
                    }


                }

                return RedirectToAction("Create");
            }
            catch(Exception ex)
            {
                string ex1 = ex.Message.ToString();
                return View();
            }
        }

        // GET: NotificationUI/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: NotificationUI/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: NotificationUI/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: NotificationUI/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
