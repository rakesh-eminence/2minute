﻿using App.Common;
using App.Models;
using App.viewmodel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace App.Controllers
{
    public class NotificationController : ApiController
    {
        private m1TestEntities db = new m1TestEntities();
        string Host = ConfigurationManager.AppSettings["Host"].ToString();
        CommonHelper obj = new CommonHelper();
        [HttpGet]
        public HttpResponseMessage GetNotification(long UserId)
        {
            List<NotificationViewModel> ObjNotificationViewModel = new List<NotificationViewModel>();
            try
            {
                ObjNotificationViewModel = db.tbl_Notification.Where(x => x.UserId == UserId).Select(dataModel =>
                                                            new NotificationViewModel()
                                                            {
                                                                NotificationId = dataModel.pkNotificationId,
                                                                Name = dataModel.Text,
                                                                ProgramId = dataModel.fkProgramId,
                                                            }).ToList();

                return obj.APIResponse(HttpStatusCode.OK, true, ObjNotificationViewModel, "", Request);
            }
            catch (Exception ex)
            {
                return obj.APIResponse(HttpStatusCode.OK, false, ex.Message, "", Request);

            }
        }
    }
}
