﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using App.Common;
using App.Models;
using PagedList;


namespace App.Controllers
{
    public class UserController : Controller
    {
        private m1TestEntities db = new m1TestEntities();
        string _SiteURL = WebConfigurationManager.AppSettings["SiteImgURL"];
        // GET: User

        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Email" ? "email_desc" : "Email";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var users = from s in db.tbl_LoginMaster
                        select s;
            if (!String.IsNullOrEmpty(searchString))
            {
                users = users.Where(s => s.Name.Contains(searchString));
            }
            switch (sortOrder)
            {
                case "name_desc":
                    users = users.OrderByDescending(s => s.Name);
                    break;
                case "Email":
                    users = users.OrderBy(s => s.EmailId);
                    break;
                case "email_desc":
                    users = users.OrderByDescending(s => s.EmailId);
                    break;
                default:  // Name ascending 
                    users = users.OrderBy(s => s.Name);
                    break;
            }

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(users.ToPagedList(pageNumber, pageSize));

            //ViewBag.CurrentSort = sortOrder;
            //return View(db.tbl_LoginMaster.ToList());
        }

        // GET: User/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_LoginMaster tbl_LoginMaster = db.tbl_LoginMaster.Find(id);
            if (tbl_LoginMaster == null)
            {
                return HttpNotFound();
            }
            return View(tbl_LoginMaster);
        }

        // GET: User/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(tbl_LoginMaster tbl_LoginMaster, HttpPostedFileBase file)
        {
            string fileName = string.Empty;
            string destinationPath = string.Empty;
            if (ModelState.IsValid)
            {
                
                HttpPostedFileBase file_Uploader = Request.Files[0] as HttpPostedFileBase;
                if (file_Uploader.ContentLength > 0)
                {
                    string GuidName = Guid.NewGuid().ToString();
                    fileName = Path.GetFileName(file_Uploader.FileName);
                    fileName = GuidName + "_" + fileName;
                    destinationPath = Path.Combine(Server.MapPath("~/Upload/UserProfile/"), fileName);
                    file_Uploader.SaveAs(destinationPath);
                    tbl_LoginMaster.ImageUrl = _SiteURL + "/UserProfile/" + fileName;
                 ////////////////////////
           





                 /////////////////////////////////////

                }
                else
                {
       
                    tbl_LoginMaster.ImageUrl = _SiteURL + "/UserProfile/defaultuser.jpg";
                
                }
                tbl_LoginMaster.Date = DateTime.Now;
                db.tbl_LoginMaster.Add(tbl_LoginMaster);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_LoginMaster);
        }

        // GET: User/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            tbl_LoginMaster tbl_LoginMaster = db.tbl_LoginMaster.Find(id);
            if (tbl_LoginMaster == null)
            {
                return HttpNotFound();
            }
            return View(tbl_LoginMaster);
        }

        // POST: User/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "pkId,Name,EmailId,Password,DeviceToken,DeviceType,FacebookId,IsActive,Date,ImageUrl,IsAdmin")] tbl_LoginMaster tbl_LoginMaster, HttpPostedFileBase file)
        {
            string fileName = string.Empty;
            string destinationPath = string.Empty;
            var Result = db.tbl_LoginMaster.Where(x => x.pkId == tbl_LoginMaster.pkId).FirstOrDefault();
            if (ModelState.IsValid)
            {
                HttpPostedFileBase file_Uploader = Request.Files[0] as HttpPostedFileBase;
                if (file_Uploader.ContentLength > 0)
                {
                    if (!Result.ImageUrl.Contains("defaultuser.jpg"))
                    {
                        int pos = Result.ImageUrl.LastIndexOf("/") + 1;
                        fileName = Result.ImageUrl.Substring(pos, Result.ImageUrl.Length - pos);
                        destinationPath = Path.Combine(Server.MapPath("~/Upload/UserProfile/"), fileName);

                        if (System.IO.File.Exists(destinationPath))
                        {
                            System.IO.File.Delete(destinationPath);
                        }

                    }


                    string GuidName = Guid.NewGuid().ToString();
                    fileName = Path.GetFileName(file_Uploader.FileName);
                    fileName =  GuidName +"_"+ fileName;
                    destinationPath = Path.Combine(Server.MapPath("~/Upload/UserProfile/"), fileName);
                    file_Uploader.SaveAs(destinationPath);
               
                    // destinationPath = Path.Combine(Server.MapPath("~/Upload/UserProfile/temporaryFilesave"), fileName);
                    // file_Uploader.SaveAs(destinationPath);
                    //string  NewdestinationPath = Path.Combine(Server.MapPath("~/Upload/UserProfile/"), fileName);
                    // ResizeImage.CropAndResizeImage(destinationPath, NewdestinationPath, fileName, 900,760,0,0,50,50);
                    // System.IO.File.Delete(destinationPath);
                    Result.ImageUrl = _SiteURL + "/UserProfile/" + fileName;
                    Result.Name = tbl_LoginMaster.Name;
                    Result.EmailId = tbl_LoginMaster.EmailId;
                    Result.IsActive = tbl_LoginMaster.IsActive;
                    Result.Date = tbl_LoginMaster.Date;
                 
                  
                }
                else
                {

                    Result.ImageUrl = Result.ImageUrl;
                    Result.Name = tbl_LoginMaster.Name;
                    Result.EmailId = tbl_LoginMaster.EmailId;
                    Result.IsActive = tbl_LoginMaster.IsActive;
                    Result.Date = tbl_LoginMaster.Date;

                   
                }



                db.Entry(Result).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_LoginMaster);
        }

        // GET: User/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_LoginMaster tbl_LoginMaster = db.tbl_LoginMaster.Find(id);
            if (tbl_LoginMaster == null)
            {
                return HttpNotFound();
            }
            return View(tbl_LoginMaster);
        }

        // POST: User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            tbl_LoginMaster tbl_LoginMaster = db.tbl_LoginMaster.Find(id);
            db.tbl_LoginMaster.Remove(tbl_LoginMaster);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
