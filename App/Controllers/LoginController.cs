﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Http.Description;
using App.Common;
using App.Filters;
using App.Models;
using App.Utility;
using App.viewmodel;
using BrainAppUtility;
using Newtonsoft.Json;

namespace App.Controllers
{
    [BasicAuthentication]
    public class LoginController : ApiController
    {
        private m1TestEntities db = new m1TestEntities();
        CommonHelper obj = new CommonHelper();
        int _Imagethumbsize = 0;
        int _imageSize = 0;

        public HttpResponseMessage GetUsers()
        {
            var res = db.tbl_LoginMaster;
            var result = new
            {
                Status = true,
                Message = res

            };
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [ResponseType(typeof(void))]
        public IHttpActionResult Putlogin(long id, tbl_LoginMaster tbl_LoginMaster)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tbl_LoginMaster.pkId)
            {
                return BadRequest();
            }

            db.Entry(tbl_LoginMaster).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!tbl_LoginMasterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }



        [HttpPost]
        public HttpResponseMessage SignUp(tbl_LoginMaster tbl_LoginMasterModel)
        {

            try
            {
                string _SiteURL = WebConfigurationManager.AppSettings["SiteImgURL"];

            
                bool status = true; string Response = string.Empty;
                if (!ModelState.IsValid)
                {
                    return obj.APIResponse(HttpStatusCode.BadRequest, false, CommonMessage.BadRequest, "", Request);
                }
                else
                {
                    var IsEmailExists = db.tbl_LoginMaster.Where(x => x.EmailId == tbl_LoginMasterModel.EmailId).FirstOrDefault();
                    tbl_LoginMasterModel.ImageUrl = _SiteURL + "/UserProfile/defaultuser.jpg";

                    if (IsEmailExists != null)
                    {
                        tbl_LoginMaster objtbl_LoginMasterModel = new tbl_LoginMaster();
                        status = false; Response = CommonMessage.EmailExists;
                        return obj.APIResponse(HttpStatusCode.OK, status, Response, objtbl_LoginMasterModel, Request);
                    }

                    tbl_LoginMasterModel.Password = Encryption.Encrypt(tbl_LoginMasterModel.Password);
                    var id = db.Proc_SaveLogin(tbl_LoginMasterModel.Name, tbl_LoginMasterModel.EmailId, tbl_LoginMasterModel.Password, tbl_LoginMasterModel.ImageUrl, tbl_LoginMasterModel.DeviceToken, tbl_LoginMasterModel.DeviceType, tbl_LoginMasterModel.FacebookId);
                    var response = db.tbl_LoginMaster.Where(x => x.EmailId == tbl_LoginMasterModel.EmailId).FirstOrDefault();

                    var Res = CreatedAtRoute("DefaultApi", new { id = tbl_LoginMasterModel.pkId }, tbl_LoginMasterModel);

                    if (Res == null) { status = false; Response = CommonMessage.FailedSignUP; } else { Response = CommonMessage.SuccessSignUp; }




                    StringBuilder sb = new StringBuilder();
                    sb.Append("Hi " + response.Name + ",");
                    sb.Append("<br/>");
                    sb.Append("You have successfully registered.");
                    sb.Append("<br/>");
                    sb.Append("Thanks,");
                    sb.Append("<br/>");
                    sb.Append("2 Minute Brain");
                    EmailHelper.SendEmail(response.EmailId, string.Empty, string.Empty, CommonMessage.SuccessSignUp, sb.ToString());
                              
                    //AweberEntry.AweberUserAdd(tbl_LoginMasterModel.EmailId, tbl_LoginMasterModel.Name);
                    return obj.APIResponse(HttpStatusCode.OK, status, Response, response, Request);
                }
     
            }
            catch (Exception ex)
            {
                tbl_LoginMaster objtbl_LoginMasterModel = new tbl_LoginMaster();
                return obj.APIResponse(HttpStatusCode.OK, false, ex.InnerException.ToString(), objtbl_LoginMasterModel, Request);
            }
        }

        [HttpPost]
        public HttpResponseMessage AddinAweber(AweberViewModel tbl_LoginMasterModel)
        {
            try
            {
                AweberEntry.AweberUserAdd(tbl_LoginMasterModel.EmailId, tbl_LoginMasterModel.Name);
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, new {}, Request);
            }
            catch (Exception ex)
            {
                return obj.APIResponse(HttpStatusCode.OK, false, "something wrong", new {}, Request);
            }
        }

        [HttpPost]
        public HttpResponseMessage SignUpWithFacebook(SignInWithThirdParty SignInWithThirdParty)
        {
            try
            {

                bool status = true; string Response = string.Empty;
                tbl_LoginMaster tbl_LoginMaster = new tbl_LoginMaster();
                if (!ModelState.IsValid)
                {
                    return obj.APIResponse(HttpStatusCode.BadRequest, false, CommonMessage.BadRequest, "", Request);
                }
                else
                {
                    var IsFacebookIdExists = db.tbl_LoginMaster.Where(x => x.FacebookId == SignInWithThirdParty.FacebookId).FirstOrDefault();
                    string strThumbnailURLfordb = null;
                    string strIamgeURLfordb = null;
                    string _SiteRoot = WebConfigurationManager.AppSettings["SiteImgPath"];
                    string _SiteURL = WebConfigurationManager.AppSettings["SiteImgURL"];

                    string strThumbnailImage = SignInWithThirdParty.ImageUrl;
                    if (SignInWithThirdParty.ThirdPartyType == ThirdPartyType.Facebook)
                    {
                        if (SignInWithThirdParty.ImageUrl != null && SignInWithThirdParty.ImageUrl != "")
                        {
                            try
                            {
                                var OldFilename = new String[10];
                                if (IsFacebookIdExists.ImageUrl.Contains('/'))
                                {
                                    OldFilename = IsFacebookIdExists.ImageUrl.Split('/');
                                }
                                var filePath = HttpContext.Current.Server.MapPath("~/Upload/UserProfile/" + OldFilename[5]);
                                string strTempImageSave = ResizeImage.Download_Image(SignInWithThirdParty.ImageUrl);
                                string profileFilePath = _SiteURL + "/UserProfile/" + strTempImageSave;
                                strIamgeURLfordb = profileFilePath;
                                SignInWithThirdParty.ImageUrl = profileFilePath;

                                if (File.Exists(filePath))
                                {
                                    File.Delete(filePath);
                                }
                            }
                            catch (Exception ex)
                            {
                                strThumbnailURLfordb = strThumbnailImage;
                                strIamgeURLfordb = strThumbnailImage;
                            }
                        }
                        else
                        {
                            SignInWithThirdParty.ImageUrl = _SiteURL + "/UserProfile/defaultuser.jpg";
                        }
                        if (IsFacebookIdExists != null)
                        {
                            IsFacebookIdExists.ImageUrl = SignInWithThirdParty.ImageUrl;
                            status = true; Response = CommonMessage.SuccessSignUp;
                            db.Entry(IsFacebookIdExists).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            long userID = IsFacebookIdExists.pkId;
                            IsFacebookIdExists = db.tbl_LoginMaster.Find(userID);
                        }
                        else
                        {
                            db.Proc_SaveLogin(SignInWithThirdParty.Name, SignInWithThirdParty.EmailId, SignInWithThirdParty.Password, SignInWithThirdParty.ImageUrl, SignInWithThirdParty.DeviceToken, SignInWithThirdParty.DeviceType, SignInWithThirdParty.FacebookId);
                            status = true; Response = CommonMessage.SuccessSignUp;
                            IsFacebookIdExists = db.tbl_LoginMaster.Where(x => x.FacebookId == SignInWithThirdParty.FacebookId).FirstOrDefault();

                        }



                        return obj.APIResponse(HttpStatusCode.OK, status, Response, IsFacebookIdExists, Request);

                    }
                    //else
                    //{
                    //    db.Proc_SaveLogin(SignInWithThirdParty.Name, SignInWithThirdParty.EmailId, SignInWithThirdParty.Password, SignInWithThirdParty.DeviceToken, SignInWithThirdParty.DeviceType, SignInWithThirdParty.FacebookId);
                    //}
                    //var response = db.tbl_LoginMaster.Where(x => x.EmailId == SignInWithThirdParty.EmailId).FirstOrDefault();
                    var Res = CreatedAtRoute("DefaultApi", new { id = SignInWithThirdParty.Id }, SignInWithThirdParty);
                    if (Res == null) { status = false; Response = CommonMessage.FailedSignUP; } else { Response = CommonMessage.SuccessLogin; }

                    return obj.APIResponse(HttpStatusCode.OK, status, CommonMessage.SuccessSignUp, IsFacebookIdExists, Request);
                }
            }
            catch (Exception ex)
            {
                return obj.APIResponse(HttpStatusCode.OK, false, CommonMessage.ExceptionMsg, "", Request);
            }
        }

        //[HttpGet]
        //public HttpResponseMessage SignUp(string Name, string EmailId, string password)
        //{
        //    try
        //    {
        //        tbl_LoginMaster tbl_LoginMaster = new tbl_LoginMaster();
        //        tbl_LoginMaster.Name = Name;
        //        tbl_LoginMaster.EmailId = EmailId;
        //        tbl_LoginMaster.Password = password;
        //        bool status = true; string Response = "Success";
        //        if (!ModelState.IsValid)
        //        {
        //            return APIResponse(HttpStatusCode.BadRequest, false, "Bad Request");
        //        }
        //        else
        //        {

        //            db.tbl_LoginMaster.Add(tbl_LoginMaster);
        //            db.SaveChanges();

        //            var Res = CreatedAtRoute("DefaultApi", new { id = tbl_LoginMaster.pkId }, tbl_LoginMaster);
        //            if (Res == null) { status = false; Response = "Something Wrong"; }

        //            return APIResponse(HttpStatusCode.OK, status, Response);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return APIResponse(HttpStatusCode.OK, false, ex.Message);
        //    }
        //}

        [HttpPost]
        public HttpResponseMessage GetLogin(tbl_LoginMaster tbl_LoginMaster)
        {
            try
            {
                bool status = true; string Response = string.Empty; tbl_LoginMaster loginMaster = new tbl_LoginMaster();
                LoginViewModel objLoginViewModel = new LoginViewModel();
                tbl_LoginMaster.Password = Encryption.Encrypt(tbl_LoginMaster.Password);
                var res = db.Proc_GetLogin(tbl_LoginMaster.EmailId, tbl_LoginMaster.Password, tbl_LoginMaster.DeviceType, tbl_LoginMaster.DeviceToken).FirstOrDefault();

                if (res == null) { status = false; Response = CommonMessage.NotAuthorised; }
                else
                {
                    bool IsSurveySubmittedTemp = res.IsSurveySubmitted == 0 ? false : true;
                    objLoginViewModel = db.tbl_LoginMaster.Where(x => x.EmailId == tbl_LoginMaster.EmailId).Select(y => new LoginViewModel()
                    {
                        pkId = y.pkId,
                        Name = y.Name,
                        EmailId = y.EmailId,
                        ImageUrl = y.ImageUrl,
                        DeviceToken = y.DeviceToken,
                        DeviceType = y.DeviceType,
                        IsAdmin = y.IsAdmin,
                        IsSurveySubmitted = IsSurveySubmittedTemp

                    }).FirstOrDefault();
                    Response = CommonMessage.SuccessLogin;
                }
                return obj.APIResponse(HttpStatusCode.OK, status, Response, objLoginViewModel, Request);
            }
            catch (Exception)
            {
                return obj.APIResponse(HttpStatusCode.OK, false, CommonMessage.ExceptionMsg, "", Request);
            }
        }

        [HttpPost]
        public HttpResponseMessage UpdateUserProfile()
        {
            try
            {
                var httpContext = (HttpContextWrapper)Request.Properties["MS_HttpContext"];
                int pkId = 0;
                string Name = "";

                if (httpContext.Request.Form["pkId"] == "")
                {

                    return obj.APIResponse(HttpStatusCode.OK, false, "User Not Found", "", Request);
                }
                else
                {
                    pkId = Convert.ToInt32(httpContext.Request.Form["pkId"].ToString());
                }
                if (httpContext.Request.Form["Name"] != "")
                {
                    Name = httpContext.Request.Form["Name"].ToString();

                }

                //string strIamgeURLfordb = null;
                string _SiteRoot = WebConfigurationManager.AppSettings["SiteImgPath"];
                string _SiteURL = WebConfigurationManager.AppSettings["SiteImgURL"];
                if (!ModelState.IsValid)
                {
                    return obj.APIResponse(HttpStatusCode.BadRequest, false, CommonMessage.BadRequest, "", Request);
                }
                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }
                tbl_LoginMaster entity = null;
                entity = db.tbl_LoginMaster.Find(pkId);

                if (entity != null)
                {
                    //entity.ImageURL = "";
                    entity.Name = Name;
                    if (HttpContext.Current.Request.Files.Count > 0)
                    {
                        ImageResponse imgResponse = MultipartFiles.GetMultipartImage(HttpContext.Current.Request.Files, "Image", "UserProfile", _Imagethumbsize, _Imagethumbsize, _imageSize, _imageSize, true, true, true, "UserProfile");

                        if (imgResponse.IsSuccess == true)
                        {
                            var filePath = entity.ImageUrl;
                            string fileName = HttpContext.Current.Server.MapPath("~/Upload/UserProfile/" + Path.GetFileName(filePath));
                            if (File.Exists(fileName))
                            {
                                File.Delete(fileName);
                            }
                            //  entity.ImageUrl = imgResponse.ImageURL;
                            entity.ImageUrl = _SiteURL + "/UserProfile/" + imgResponse.GuidImage;
                        }
                        else
                        {
                            return obj.APIResponse(HttpStatusCode.BadRequest, false, CommonMessage.BadRequest, "", Request);
                        }

                    }
                    else
                    {
                        if (string.IsNullOrEmpty(entity.ImageUrl))
                        {
                            entity.ImageUrl = _SiteURL + "/UserProfile/defaultuser.jpg";
                        }

                    }

                    db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    long userID = entity.pkId;
                    entity = db.tbl_LoginMaster.Find(userID);
                    return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, entity, Request);
                }
                else
                {
                    return obj.APIResponse(HttpStatusCode.BadRequest, false, "User Not Found", "", Request);
                }

            }
            catch (Exception ex)
            {

                return obj.APIResponse(HttpStatusCode.BadRequest, false, ex.Message.ToString(), "", Request); ;
            }
        }

        [HttpPost]
        public HttpResponseMessage ChangePassword(ChnagePassWordviewModel model)
        {
            try
            {
                bool status = true; string Response = string.Empty;
                model.NewPassword = Encryption.Encrypt(model.NewPassword);
                model.OldPassword = Encryption.Encrypt(model.OldPassword);
                var res = db.Proc_ChangePassword(model.UserId, model.OldPassword, model.NewPassword).FirstOrDefault();

                if (res != 1) { status = false; Response = CommonMessage.FailedUpdatePassword; } else { Response = CommonMessage.SuccessPWDChanged; }
                return obj.APIResponse(HttpStatusCode.OK, status, Response, "", Request);
            }
            catch (Exception ex)
            {
                return obj.APIResponse(HttpStatusCode.OK, false, CommonMessage.ExceptionMsg, ex.Message.ToString(), Request);
            }
        }

        [HttpGet]
        public HttpResponseMessage ResetPassword(string EmailId)
        {
            try
            {
                bool status = true; string Response = string.Empty;
                var res = db.tbl_LoginMaster.Where(x => x.EmailId == EmailId).Select(x => new { x.EmailId, x.Name }).FirstOrDefault();

                if (res == null)
                {
                    status = false;
                    Response = CommonMessage.FailedEmailVerification;
                }
                else
                {
                    string OTP_ResetPassword = CommonMessage.GenerateRandomNo().ToString();
                    //--------------------Save in DB-----------------------------//
                    //tbl_OTPMaster _otpDetails = new tbl_OTPMaster();
                    // _otpDetails.EmailId = EmailId;
                    // _otpDetails.OTP = Convert.ToString(OTP_ResetPassword);
                    // _otpDetails.Type = (int)OTPType.PasswordReset;
                    // _otpDetails.Date = DateTime.Now;
                    // _otpDetails.ValidUpTo = 15;
                    //db.tbl_OTPMaster.Add(_otpDetails);
                    // db.SaveChanges();
                    // db.Proc_SaveOTP(_otpDetails);

                 //   db.Proc_SaveOTP(res.EmailId, Convert.ToString(OTP_ResetPassword), (int)OTPType.PasswordReset);
                    //---------------------------------------------------------//
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Hi " + res.Name + ",");
                    sb.Append("<br/>");
                    sb.Append("You have requested a password reset, please use below  password.");
                    sb.Append("Please ignore this email if you did not request a password change.");
                    sb.Append("<br/>");
                    sb.Append("Login Password : <b>" + OTP_ResetPassword.ToString() + "<b>");
                    sb.Append("<br/>");
                    sb.Append("<br/>");
                    sb.Append("Thanks,");
                    sb.Append("<br/>");
                    sb.Append("2 Minute Brain");

                    if (EmailHelper.SendEmail(res.EmailId, string.Empty, string.Empty, CommonMessage.Subject_PasswordReset, sb.ToString()))
                    {
                        Response = CommonMessage.PasswordReset;
                        status = true;
                        OTP_ResetPassword = Encryption.Encrypt(OTP_ResetPassword);
                        //temp added this line needed OTP screen // remove this line if OTP screen is ready
                        db.Proc_ValidateResetPassword(EmailId, OTP_ResetPassword.ToString(), (int)OTPType.PasswordReset).FirstOrDefault();
                    }
                    else
                    {
                        Response = CommonMessage.ExceptionMsg;
                        status = false;
                    }
                }
                return obj.APIResponse(HttpStatusCode.OK, status, Response, "", Request);
            }
            catch (Exception EX)
            {
                return obj.APIResponse(HttpStatusCode.OK, false, CommonMessage.ExceptionMsg, "", Request);
            }
        }

        //call this api when OTP screen is ready
        [HttpGet]
        public HttpResponseMessage Validate_ResetPassword(string EmailId, string OTP)
        {
            try
            {
                bool status = true; string Response = string.Empty;
                OTP = Encryption.Encrypt(OTP);
                var res = db.Proc_ValidateResetPassword(EmailId, OTP, (int)OTPType.PasswordReset).FirstOrDefault();

                if (res != 1) { status = false; Response = CommonMessage.FailedPWDVerification; } else { Response = CommonMessage.SuccessLogin; }
                return obj.APIResponse(HttpStatusCode.OK, status, Response, "", Request);
            }
            catch (Exception)
            {
                return obj.APIResponse(HttpStatusCode.OK, false, CommonMessage.ExceptionMsg, "", Request);
            }
        }


        [HttpGet]
        public HttpResponseMessage GetUserById(long UserId)
        {
            try
            {
                var res = db.tbl_LoginMaster.Where(x => x.pkId == UserId).FirstOrDefault();
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, res, Request);
            }
            catch (Exception ex)
            {
                return obj.APIResponse(HttpStatusCode.OK, false, ex.Message, "", Request);
            }
        }

        [HttpGet]
        public HttpResponseMessage SendNotificationopentrack()
        {
            m1TestEntities db;
            try
            {


                List<Proc_GetAllUserForUnlockTrack_Result> ObjGetUserList = new List<Proc_GetAllUserForUnlockTrack_Result>();
                using (db = new m1TestEntities())
                {
                    ObjGetUserList = db.Proc_GetAllUserForUnlockTrack().ToList();
                    if(ObjGetUserList != null)
                    {
                        if(ObjGetUserList[0].Dayscount == 0 )
                        {
                            return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, "0", Request);

                        }

                    }

                }
                using (db = new m1TestEntities())
                {
                    long UserId = 0;
                    PushNotifications ObjPushNotification = new PushNotifications();
                    foreach (Proc_GetAllUserForUnlockTrack_Result item in ObjGetUserList)
                    {
                        if (item.DeviceType == "Android")
                        {


                            if (UserId != item.UserId)
                            {
                                var ObjUpdateTrack = db.Proc_UnlockTrackforUser(item.UserId, item.fkProgramId).FirstOrDefault();
                                if (!string.IsNullOrEmpty(ObjUpdateTrack.BasicTrackRelease))
                                {
                                    ObjPushNotification.SendNotification_Android(item.DeviceToken, CommonMessage.BasicTrackRelease, "Track", item.fkProgramId);
                                    db.proc_InsertNotification(CommonMessage.BasicTrackRelease, item.UserId, item.fkProgramId);
                                }
                                if (!string.IsNullOrEmpty(ObjUpdateTrack.BonusTrackRelease))
                                {
                                     ObjPushNotification.SendNotification_Android(item.DeviceToken, CommonMessage.BonusTrackRelease, "Bonus", item.fkProgramId);
                                    db.proc_InsertNotification(CommonMessage.BonusTrackRelease, item.UserId, item.fkProgramId);
                                }
                            }
                            else
                            {   // only open track not send notification 
                                var ObjUpdateTrack1 = db.Proc_UnlockTrackforUser(item.UserId, item.fkProgramId).FirstOrDefault();
                            }

                        }
                        else if (item.DeviceType == "IOS")
                        {
                           
                            if (UserId != item.UserId)
                            {
                                var ObjUpdateTrack = db.Proc_UnlockTrackforUser(item.UserId, item.fkProgramId).FirstOrDefault();
                                if (!string.IsNullOrEmpty(ObjUpdateTrack.BasicTrackRelease))
                                {
                                     ObjPushNotification.SendNotification_IOS(item.DeviceToken, CommonMessage.BasicTrackRelease, "Track", item.fkProgramId);
                                    db.proc_InsertNotification(CommonMessage.BasicTrackRelease, item.UserId, item.fkProgramId);
                                }
                                if (!string.IsNullOrEmpty(ObjUpdateTrack.BonusTrackRelease))
                                {
                                     ObjPushNotification.SendNotification_IOS(item.DeviceToken, CommonMessage.BonusTrackRelease, "Bonus", item.fkProgramId);
                                    db.proc_InsertNotification(CommonMessage.BonusTrackRelease, item.UserId, item.fkProgramId);
                                }
                            }
                            else
                            {   // only open track not send notification 
                                var ObjUpdateTrack1 = db.Proc_UnlockTrackforUser(item.UserId, item.fkProgramId).FirstOrDefault();
                            }
                        }
                        UserId = item.UserId;
                    }


                }
            }
            catch (Exception ex)
            {
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, "", Request);

            }

            return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success + DateTime.Now.ToString(), "", Request);
        }



        // DELETE: api/login/5
        [ResponseType(typeof(tbl_LoginMaster))]
        public IHttpActionResult Deletelogin(long id)
        {
            tbl_LoginMaster tbl_LoginMaster = db.tbl_LoginMaster.Find(id);
            if (tbl_LoginMaster == null)
            {
                return NotFound();
            }

            db.tbl_LoginMaster.Remove(tbl_LoginMaster);
            db.SaveChanges();

            return Ok(tbl_LoginMaster);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool tbl_LoginMasterExists(long id)
        {
            return db.tbl_LoginMaster.Count(e => e.pkId == id) > 0;
        }



    }
}