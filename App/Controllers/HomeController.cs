﻿using App.Models;
using App.viewmodel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace App.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private m1TestEntities db = new m1TestEntities();


        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            DashboardViewModel dashboard = new DashboardViewModel();

            dashboard.users_count = db.tbl_LoginMaster.Count();
            dashboard.programs_count = db.tbl_ProgramMaster.Count();
            dashboard.CommingSoon = db.tbl_ProgramMaster.Where(x => x.IsActive == false).Count();

            var Result = db.Proc_GetDataforRepresentationHomePage().ToList();


            long? SumTotalcourse = Result.Sum(x => x.TotalCourseOpen);

            dashboard.ProgramviewModelList = new List<ProgramviewModel>();


            foreach (var item in Result)
            {
                ProgramviewModel obj = new ProgramviewModel();
                //dashboard.ProgramviewModelList[i].PercentageTotalOpenCourse = (Convert.ToDecimal(item.TotalCourseOpen / SumTotalcourse));
                obj.indexLabel = item.Name;
                obj.label = (((float)item.TotalCourseOpen / (float)SumTotalcourse) * 100).ToString("0.00") + "%";
                obj.y = (((float)item.TotalCourseOpen / (float)SumTotalcourse) * 100);
                dashboard.ProgramviewModelList.Add(obj);
            }







            //dashboard.patients_count = db.Patients.Count();
            return View(dashboard);
            //db.tbl_LoginMaster.Count
            //return View();
        }

        public ActionResult SP_GetLogin()
        {
            return View(db.Proc_GetUser());
        }
    }
}
