﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using App.Common;
using App.Models;
using App.viewmodel;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using System.Web;

namespace App.Controllers
{
    public class PlayListUserMobileAppController : ApiController
    {
        private m1TestEntities db = new m1TestEntities();
        CommonHelper obj = new CommonHelper();


        [HttpPost]
        public HttpResponseMessage CreatePlayListAndTrack(UserPlayListViewModel Model)
        {
            try
            {
                tbl_UserPlayList Objtbl_UserPlayList = new tbl_UserPlayList();
                long pkUserPlayListId = 0;
                if (Model.PlayListId == 0)
                {
                    var Result = db.tbl_UserPlayList.Where(x => x.Name == Model.PlayListName && x.UserId == Model.UserId).FirstOrDefault();
                    if (Result != null)
                    {
                        return obj.APIResponse(HttpStatusCode.OK, false, CommonMessage.PlayListDuplicate, "", Request);
                    }
                    //var Result2 = db.tbl_UserPlayList.Where(x => x.UserId == Model.UserId).LastOrDefault();
                    //var Result2 = db.tbl_UserPlayList.Where(x => x.UserId == Model.UserId).OrderByDescending(y => y.OrderByplayList).FirstOrDefault();
                    //int? OrderByPlayList = 0;
                    //if (Result2 != null)
                    //{
                    //    OrderByPlayList = Result2.OrderByplayList + 1;
                    //}
                    //else
                    //{
                    //    OrderByPlayList = 1;
                    //}
                    Objtbl_UserPlayList.Name = Model.PlayListName;
                    Objtbl_UserPlayList.UserId = Model.UserId;
                    Objtbl_UserPlayList.CreatedDate = DateTime.UtcNow;
                    Objtbl_UserPlayList.IsActive = true;
                  //  Objtbl_UserPlayList.OrderByplayList = OrderByPlayList;
                    db.tbl_UserPlayList.Add(Objtbl_UserPlayList);
                    db.SaveChanges();
                    pkUserPlayListId = Objtbl_UserPlayList.pkUserPlayListId;
                    return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.PlayListCreateSuccessfully, "", Request);


                }
                else if (Model.PlayListId > 0)
                {
                    pkUserPlayListId = Model.PlayListId;

                }

                if (Model.PlayTrackList != null)
                {
                    int? OrderByPlayList = 0;
                    foreach (PlayListTrackViewModel item in Model.PlayTrackList)
                    {
                        var Result1 = db.tbl_UsersProgPlayListDetail.Where(x => x.fkProgramPlayListId == item.PlayLIstTrackId && x.UserId == Model.UserId).FirstOrDefault();
                        if (Result1 != null)
                        {
                            Result1.IsFav = true;
                            Result1.fkCustomUserPlayListId = Model.PlayListId;
                            db.Entry(Result1).State = EntityState.Modified;
                            db.SaveChanges();


                            var Resulst4 = db.tbl_PlayListTrackOrder.Where(x => x.UserId == Model.UserId && x.fkCustomUserPlayListId == Model.PlayListId ).
                                                                       OrderByDescending(y => y.OrderByIndex).FirstOrDefault();
                           
                            if (Resulst4 != null)
                            {
                                OrderByPlayList = Resulst4.OrderByIndex + 1;
                            }
                            else
                            {
                                OrderByPlayList = 1;
                            }

                            tbl_PlayListTrackOrder objtbl_PlayListTrackOrder = new tbl_PlayListTrackOrder();

                            objtbl_PlayListTrackOrder.fkCustomUserPlayListId = Result1.fkCustomUserPlayListId;
                            objtbl_PlayListTrackOrder.fkProgramPlayListId = Result1.fkProgramPlayListId;
                            objtbl_PlayListTrackOrder.UserId = Model.UserId;
                            objtbl_PlayListTrackOrder.OrderByIndex = OrderByPlayList;
                            db.tbl_PlayListTrackOrder.Add(objtbl_PlayListTrackOrder);
                            db.SaveChanges();

                 
                        }



                    }

                    return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.trackAddedsuccessfully, "", Request);
                }
            }

            catch (Exception ex)
            {

                return obj.APIResponse(HttpStatusCode.OK, true, ex.Message.ToString(), "", Request); ;
            }

            return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, "", Request);

        }


        [HttpPost]
        public HttpResponseMessage CreatePlayListAndTrackForAndroid(UserPlayListViewModel Model)
        {
            try
            {
                tbl_UserPlayList Objtbl_UserPlayList = new tbl_UserPlayList();
                long pkUserPlayListId = 0;
                if (Model.PlayListId == 0)
                {
                    var Result = db.tbl_UserPlayList.Where(x => x.Name == Model.PlayListName && x.UserId == Model.UserId).FirstOrDefault();
                    if (Result != null)
                    {
                        return obj.APIResponse(HttpStatusCode.OK, false, CommonMessage.PlayListDuplicate, "", Request);
                    }
                    Objtbl_UserPlayList.Name = Model.PlayListName;
                    Objtbl_UserPlayList.UserId = Model.UserId;
                    Objtbl_UserPlayList.CreatedDate = DateTime.UtcNow;
                    Objtbl_UserPlayList.IsActive = true;
                    db.tbl_UserPlayList.Add(Objtbl_UserPlayList);
                    db.SaveChanges();
                    pkUserPlayListId = Objtbl_UserPlayList.pkUserPlayListId;
                   // return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.PlayListCreateSuccessfully, "", Request);


                }
                else if (Model.PlayListId > 0)
                {
                    pkUserPlayListId = Model.PlayListId;

                }

                if (Model.PlayTrackList != null)
                {
                    foreach (PlayListTrackViewModel item in Model.PlayTrackList)
                    {
                        var Result1 = db.tbl_UsersProgPlayListDetail.Where(x => x.fkProgramPlayListId == item.PlayLIstTrackId && x.UserId == Model.UserId).FirstOrDefault();
                        Result1.IsFav = true;
                        Result1.fkCustomUserPlayListId = pkUserPlayListId;
                       // db.Entry(Result1).State = EntityState.Modified;
                        db.Entry(Result1).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }

                    return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.trackAddedsuccessfully, "", Request);
                }
            }

            catch (Exception ex)
            {

                return obj.APIResponse(HttpStatusCode.OK, true, ex.Message.ToString(), "", Request); ;
            }

            return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, "", Request);

        }

        [HttpGet]
        public HttpResponseMessage GetAllPlayList(long UserId)
        {
            List<UserPlayListViewModel> Result = new List<UserPlayListViewModel>();
            try
            {
                Result = (from playList in db.tbl_UserPlayList
                          where playList.UserId == UserId
                          select new UserPlayListViewModel()
                          {
                              PlayListName = playList.Name,
                              PlayListId = playList.pkUserPlayListId,
                              UserId = playList.UserId.HasValue ? playList.UserId.Value : 0,
                              OrderByPlayList = playList.OrderByplayList

                          }).ToList();

            }
            catch (Exception)
            {

                throw;
            }

            return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, Result, Request);
        }

        [HttpGet]
        public HttpResponseMessage GetMyPlayListWithTrack(long UserId, long PlayListId)
        {
            List<UserCustomPlayListandTrackViewModel> Result = new List<UserCustomPlayListandTrackViewModel>();
            var request = HttpContext.Current.Request;
            string baseUrl = request.Url.Scheme + "://" + request.Url.Authority + '/';
            try
            {
                Result = (from UserplayList in db.tbl_UserPlayList
                          join UserPlayListDetail in db.tbl_UsersProgPlayListDetail on
                          UserplayList.pkUserPlayListId equals UserPlayListDetail.fkCustomUserPlayListId
                          join ProgramPlayList in db.tbl_ProgramPlayList on UserPlayListDetail.fkProgramPlayListId
                          equals ProgramPlayList.pkProgramPlayListId

                          where UserPlayListDetail.fkCustomUserPlayListId == PlayListId && UserplayList.UserId == UserId
                          select new UserCustomPlayListandTrackViewModel()
                          {
                              PlayListName = UserplayList.Name,
                              PlayListId = UserplayList.pkUserPlayListId,
                              ProgramId = ProgramPlayList.fkProgramId,
                              AudioTrackId = ProgramPlayList.pkProgramPlayListId,
                               TrackArtist = ProgramPlayList.TrackArtist,
                              TrackName = ProgramPlayList.PlayList,
                              TrackDescription = ProgramPlayList.Description,
                              TrackUrl = baseUrl + "Upload/" + ProgramPlayList.PlayListUrl,
                              TrackTimeCount = ProgramPlayList.TrackDuration,
                              Favourite = UserPlayListDetail.IsFav,
                              OrderByIndex = db.tbl_PlayListTrackOrder.FirstOrDefault(x => x.fkProgramPlayListId == UserPlayListDetail.fkProgramPlayListId && x.fkCustomUserPlayListId == UserplayList.pkUserPlayListId).OrderByIndex

                          }).ToList();

                Result = Result.OrderBy(x => x.OrderByIndex).ToList();
            }
            catch (Exception ex)
            {

                return obj.APIResponse(HttpStatusCode.OK, true, ex.Message, Result, Request);
            }

            return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, Result, Request);
        }

        [HttpPost]
        public HttpResponseMessage DeletePlayList(UserPlayListViewModel ModelList)
        {
            try
            {
                if (ModelList != null)
                {

                    foreach (var item in ModelList.UserPlayList)
                    {
                        tbl_UserPlayList Objtbl_UserPlayList = new tbl_UserPlayList();
                        Objtbl_UserPlayList = db.tbl_UserPlayList.Find(item.PlayListId);
                        if (Objtbl_UserPlayList != null)
                        {
                            db.tbl_UserPlayList.Remove(Objtbl_UserPlayList);
                            db.SaveChanges();
                            List<tbl_UsersProgPlayListDetail> Result1 = db.tbl_UsersProgPlayListDetail.Where(x => x.fkCustomUserPlayListId == item.PlayListId && x.UserId == item.UserId).ToList();
                            foreach (var item1 in Result1)
                            {

                                item1.IsFav = false;
                                item1.fkCustomUserPlayListId = 0;
                                db.Entry(item1).State = EntityState.Modified;
                                db.SaveChanges();
                            }

                        }

                    }


                }


            }

            catch (Exception ex)
            {

                return obj.APIResponse(HttpStatusCode.OK, true, ex.Message.ToString(), "", Request); ;
            }

            return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.PlayListRemoveSuccessfully, "", Request);

        }


        [HttpGet]
        public HttpResponseMessage RemoveFavTrackfromPlayList(long UserId, long AudioTrackId)
        {
            try
            {
                tbl_UserPlayList Objtbl_UserPlayList = new tbl_UserPlayList();

                var Result1 = db.tbl_UsersProgPlayListDetail.Where(x => x.fkProgramPlayListId == AudioTrackId && x.UserId == UserId).FirstOrDefault();

                if (Result1 != null)
                {
                    Result1.IsFav = false;
                    Result1.fkCustomUserPlayListId = 0;
                    db.Entry(Result1).State = EntityState.Modified;
                    db.SaveChanges();
                }

            }

            catch (Exception ex)
            {

                return obj.APIResponse(HttpStatusCode.OK, true, ex.Message.ToString(), "", Request); ;
            }

            return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, "", Request);

        }


        [HttpPost]
        public HttpResponseMessage ChangeTracksOrder(TrackOrderModel ModelList)
        {

            try
            {
                tbl_PlayListTrackOrder objtbl_PlayListTrackOrder = new tbl_PlayListTrackOrder();
                foreach (var item in ModelList.TrackModelList)
                {
                    var Result = db.tbl_PlayListTrackOrder.Where(x => x.UserId == ModelList.UserId && x.fkProgramPlayListId == item.TrackId && x.fkCustomUserPlayListId == ModelList.PlayListId).FirstOrDefault();
                    if (Result != null)
                    {
                        Result.OrderByIndex = item.Index;
                        db.Entry(Result).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }

                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, "", Request); ;



            }
            catch (Exception ex)
            {

                return obj.APIResponse(HttpStatusCode.OK, true, ex.Message.ToString(), "", Request); ;
            }

        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        

    }
}
