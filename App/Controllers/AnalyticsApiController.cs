﻿using App.Common;
using App.Filters;
using App.Models;
using App.viewmodel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace App.Controllers
{
    [BasicAuthentication]
    public class AnalyticsApiController : ApiController
    {
        private m1TestEntities db;
        string Host = ConfigurationManager.AppSettings["Host"].ToString();
        CommonHelper obj = new CommonHelper();

        [HttpPost]
        public HttpResponseMessage AddPopularTimespendProgramAndTrack(PopularTrackProgramDtoModel Model)
        {
            try
            {
                //What courses and tracks have the most time spent/most popular. 
                using (db = new m1TestEntities())
                {
                    tbl_Analytic objtbl_Analytic = new tbl_Analytic();
                    objtbl_Analytic.fkProgramId = Model.ProgramId;
                    objtbl_Analytic.fkProgramPlayListId = Model.TrackId;
                    objtbl_Analytic.UserId = Model.UserId;
                    objtbl_Analytic.CreatedDate = DateTime.Now;
                    objtbl_Analytic.StartTime = DateTime.Now;
                    db.tbl_Analytic.Add(objtbl_Analytic);
                    db.SaveChanges();

                }
              
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success,"", Request);
            }
            catch (Exception ex)
            {

                CommonMessage.Error = ex.Message.ToString();
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Error, "", Request);
            }
        }

        [HttpPost]
        public HttpResponseMessage AppConsumptionRecord (APPUserDtoModel Model)
        {
            try
            {

               if(Model.IsAppOpen == true)
                // when user install a app
                using (db = new m1TestEntities())
                {
                    tbl_Appuse objtbltbl_Appuse = new tbl_Appuse();
                    objtbltbl_Appuse.UserId = Model.UserId;
                    objtbltbl_Appuse.AppStartTime = DateTime.Now;
                    db.tbl_Appuse.Add(objtbltbl_Appuse);
                    db.SaveChanges();

                }
                if (Model.IsAppOpen == false)
                    // when user install a app
                    using (db = new m1TestEntities())
                    {
                        tbl_Appuse objtbltbl_Appuse = db.tbl_Appuse.Where(x => x.UserId == Model.UserId).LastOrDefault();
                        objtbltbl_Appuse.UserId = Model.UserId;
                        objtbltbl_Appuse.AppEndTime = DateTime.Now;
                        db.tbl_Appuse.Add(objtbltbl_Appuse);
                        db.Entry(objtbltbl_Appuse).State = EntityState.Modified;
                        db.SaveChanges();

                    }

                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, "", Request);
            }
            catch (Exception ex)
            {

                CommonMessage.Error = ex.Message.ToString();
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Error, "", Request);
            }
        }

        [HttpPost]
        public HttpResponseMessage AppUninstall(APPUserDtoModel Model)
        {
            try
            {
                // when user install a app
                using (db = new m1TestEntities())
                {
                    tbl_Appuse objtbltbl_Appuse = db.tbl_Appuse.Where(x => x.UserId == Model.UserId).FirstOrDefault();
                    objtbltbl_Appuse.UserId = Model.UserId;
                    objtbltbl_Appuse.AppEndTime = DateTime.Now;
                    db.tbl_Appuse.Add(objtbltbl_Appuse);
                    db.Entry(objtbltbl_Appuse).State = EntityState.Modified;
                    db.SaveChanges();

                }

                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, "", Request);
            }
            catch (Exception ex)
            {

                CommonMessage.Error = ex.Message.ToString();
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Error, "", Request);
            }
        }


        [HttpPost]
        public HttpResponseMessage TrackListen(APPUserDtoModel Model)
        {
            try
            {
                // when user install a app
                using (db = new m1TestEntities())
                {
                    tbl_UsersProgPlayListDetail objtbl_UsersProgPlayListDetail = db.tbl_UsersProgPlayListDetail.Where(x => x.UserId == Model.UserId).FirstOrDefault();
                
                    db.tbl_UsersProgPlayListDetail.Add(objtbl_UsersProgPlayListDetail);
                    db.Entry(objtbl_UsersProgPlayListDetail).State = EntityState.Modified;
                    db.SaveChanges();

                }

                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, "", Request);
            }
            catch (Exception ex)
            {

                CommonMessage.Error = ex.Message.ToString();
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Error, "", Request);
            }
        }
    }

}
