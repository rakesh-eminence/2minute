﻿using App.Models;
using App.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace App.Controllers
{
    public class AdminLoginController : Controller
    {
        private m1TestEntities usersEntities = new m1TestEntities();
        // GET: AdminLogin
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }


        //public ActionResult Profile()
        //{
        //    return View();
        //}

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(tbl_LoginMaster user)
        {
            string message = string.Empty;
            try
            {

                user.Password = Encryption.Encrypt(user.Password);
                //  var res = usersEntities.Proc_GetLogin(user.EmailId, user.Password, user.DeviceType, user.DeviceToken).FirstOrDefault();
                tbl_LoginMaster res = usersEntities.tbl_LoginMaster.Where(x => x.EmailId == user.EmailId && x.Password == user.Password && x.IsAdmin == true ).FirstOrDefault();

                if (res == null)
                {
                    message = "Username and/or password is incorrect.";
                }
                else
                {
                    FormsAuthentication.SetAuthCookie(user.EmailId, true);
                    Session["user"] = res;
                    return RedirectToAction("Index", "Home");
              
                    //loginMaster = db.tbl_LoginMaster.Where(x => x.EmailId == tbl_LoginMaster.EmailId).FirstOrDefault();
                    //Response = CommonMessage.SuccessLogin;
                }

            }
            catch (Exception ex)
            {

                ViewBag.Message = ex.Message.ToString(); ;
            }


            ViewBag.Message = message;
            return View(user);
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "AdminLogin");
        }
    }
}