﻿using App.Models;
using Stripe;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;


namespace App.Controllers
{
    public class PaymentAdminController : Controller
    {
        private m1TestEntities db = new m1TestEntities();
        // GET: PaymentAdmin
        public ActionResult Index()
        {
            var stripePublishKey = ConfigurationManager.AppSettings["StripePublishablekey"];
            ViewBag.StripePublishKey = stripePublishKey;
            return View();
        }

        public ActionResult Charge(string stripeEmail, string stripeToken)
        {


            string PrivateKey = WebConfigurationManager.AppSettings["StripeSecretkey"];
            var customers = new CustomerService();
            var charges = new ChargeService();
            StripeConfiguration.ApiKey = PrivateKey;
            try
            {



                var customer = customers.Create(new CustomerCreateOptions
                {
                    Email = stripeEmail,
                    Source = stripeToken,
                    Name = "Rakesh",
                    Address = new AddressOptions { City = "NY", Country = "America", PostalCode = "xyz", State = "NY", Line1 = "America New York" },
                });

                var charge = charges.Create(new ChargeCreateOptions
                {
                    Amount = 500,
                    Description = "Sample Charge",
                    Currency = "usd",
                    Customer = customer.Id

                });
                db.Proc_SaveStripePaymentDetail(charge.BalanceTransactionId, charge.CustomerId, charge.Amount, 1, charge.Paid, 1, charge.PaymentMethodId);
                
                return RedirectToAction("Successfull", "PaymentAdmin");
            }
            catch (Exception ex)
            {


            }
            return View();
        }

        // GET: PaymentAdmin/Create
        public ActionResult Successfull()
        {
            return View();
        }

  
    }
}
