﻿using App.Common;
using App.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using App.Models;
using System.Configuration;
using System.IO;
using System.Web;
using App.viewmodel;
using System.Globalization;
using System.Data.Entity;

namespace App.Controllers
{
    [BasicAuthentication]
    public class ProgramTrackMobileAppController : ApiController
    {
        private m1TestEntities db = new m1TestEntities();
        string Host = ConfigurationManager.AppSettings["Host"].ToString();
        CommonHelper obj = new CommonHelper();
        [HttpGet]
        public HttpResponseMessage GetAudioPlayList(long UserId, int ProgramId)
        {
            int TypeId = 2;
            var request = HttpContext.Current.Request;
            string baseUrl = request.Url.Scheme + "://" + request.Url.Authority + '/';
            try
            {

                List<ProgramTrackViewModel> Result = new List<ProgramTrackViewModel>();

                Result = (from ProMaster in db.tbl_ProgramMaster
                          join UserProgmDetail in db.tbl_UsersProgrameDetails on
                                ProMaster.pkProgramId equals UserProgmDetail.fkProgramId
                          where UserProgmDetail.fkProgramId == ProgramId && UserProgmDetail.UserId == UserId && (UserProgmDetail.IsCourseOpen == true || ProMaster.IsProgramUnlockForAllUser == true)

                          select new ProgramTrackViewModel()
                          {
                              CourseName = ProMaster.Name,
                              CourseImage = "",
                              SessionDay = ProMaster.Session,
                              CreatedDate = DateTime.Now,
                              TrackList = (from Playlist in db.tbl_ProgramPlayList
                                           join UserplayListDetail in db.tbl_UsersProgPlayListDetail on
                                           Playlist.pkProgramPlayListId equals UserplayListDetail.fkProgramPlayListId
                                           where
                                        Playlist.fkProgramId == ProgramId && UserplayListDetail.UserId == UserId && Playlist.PlayListType == TypeId && Playlist.fkProgramId == ProgramId
                                        && ((Playlist.IsBonusTrack == null ? false : Playlist.IsBonusTrack) == false)
                                           select new TrackListViewModel()
                                           {
                                               ProgramId = Playlist.fkProgramId,
                                               AudioTrackId = Playlist.pkProgramPlayListId,
                                               TrackName = Playlist.PlayList,
                                               TrackArtist = Playlist.TrackArtist,
                                               TrackUrl = baseUrl + "Upload/" + Playlist.PlayListUrl,
                                               PlayListType = Playlist.PlayListType,
                                               TrackTimeCount = Playlist.TrackDuration,
                                               IsLocked = UserplayListDetail.IsLocked ?? false,
                                               Favourite = UserplayListDetail.IsFav,
                                               TrackDescription = Playlist.Description,
                                               ShortDescription = Playlist.Description.Substring(0, 150),
                                               ValidDate= UserplayListDetail.ValidDate,

                                           }).ToList(),

                          }).ToList();

                Result[0].CreatedDate = Result[0].TrackList[0].ValidDate;

                //List<TrackListViewModel> TextTrackList = (from Playlist in db.tbl_ProgramPlayList
                //                                          join UserplayListDetail in db.tbl_UsersProgPlayListDetail on
                //                                          Playlist.pkProgramPlayListId equals UserplayListDetail.fkProgramPlayListId
                //                                          where Playlist.fkProgramId == ProgramId && UserplayListDetail.UserId == UserId && Playlist.PlayListType == 1
                //                                          && Playlist.fkProgramId == ProgramId && Playlist.IsBonusTrack == false
                //                                          select new TrackListViewModel()
                //                                          {
                //                                              ProgramId = Playlist.fkProgramId,
                //                                              AudioTrackId = Playlist.pkProgramPlayListId,
                //                                              TrackName = Playlist.PlayList,
                //                                              TrackArtist = Playlist.TrackArtist,
                //                                              PlayListType = Playlist.PlayListType,
                //                                              TrackTimeCount = Playlist.TrackDuration,
                //                                              IsLocked = UserplayListDetail.IsLocked ?? false,
                //                                              Favourite = UserplayListDetail.IsFav,
                //                                              ShortDescription = Playlist.Description.Substring(0, 150),
                //                                              LongDescription = Playlist.Description,

                //                                          }).ToList();

                //int i = 0;
                //foreach (var y in TextTrackList)
                //{
                //    if(Result[0].TrackList.Count > i+1 || Result[0].TrackList.Count == i + 1) {
                //        Result[0].TrackList[i].LongDescription = TextTrackList[i].LongDescription.ToString();
                //    }
                //    else
                //    {
                //        TrackListViewModel item = new TrackListViewModel();
                //        item.ProgramId = y.ProgramId;
                //        item.TrackName = y.TrackName;
                //        item.AudioTrackId = y.AudioTrackId;
                //        item.LongDescription = y.LongDescription;
                //        Result[0].TrackList.Add(item);
                //    }


                //    i++;
                //}


                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, Result, Request);
            }
            catch (Exception ex)
            {
                CommonMessage.Error = ex.Message.ToString();
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Error, "", Request);
            }
            finally
            {

            }
        }

        [HttpGet]
        public HttpResponseMessage GetTextTrack(long UserId, int ProgramId)
        {
            List<TrackListViewModel> TextTrackList = new List<TrackListViewModel>();
            int TypeId = 1;
            var request = HttpContext.Current.Request;
            string baseUrl = request.Url.Scheme + "://" + request.Url.Authority + '/';
            try
            {


                List<ProgramTrackViewModel> Result = new List<ProgramTrackViewModel>();
                Result = (from ProMaster in db.tbl_ProgramMaster
                          join UserProgmDetail in db.tbl_UsersProgrameDetails on
                                ProMaster.pkProgramId equals UserProgmDetail.fkProgramId
                          where UserProgmDetail.fkProgramId == ProgramId && UserProgmDetail.UserId == UserId && UserProgmDetail.IsCourseOpen == true

                          select new ProgramTrackViewModel()
                          {
                              CourseName = ProMaster.Name,
                              CourseImage = "",
                              SessionDay = ProMaster.Session,
                              CreatedDate = UserProgmDetail.CreatedDate,
                              TrackList = (from Playlist in db.tbl_ProgramPlayList
                                           join UserplayListDetail in db.tbl_UsersProgPlayListDetail on
                                           Playlist.pkProgramPlayListId equals UserplayListDetail.fkProgramPlayListId
                                           where
                                        Playlist.fkProgramId == ProgramId && UserplayListDetail.UserId == UserId && Playlist.PlayListType == TypeId && Playlist.fkProgramId == ProgramId
                                        && ((Playlist.IsBonusTrack == null ? false : Playlist.IsBonusTrack) == false)
                                           select new TrackListViewModel()
                                           {
                                               ProgramId = Playlist.fkProgramId,
                                               AudioTrackId = Playlist.pkProgramPlayListId,
                                               TrackName = Playlist.PlayList,
                                               TrackArtist = Playlist.TrackArtist,
                                               TrackUrl = baseUrl + "Upload/" + Playlist.PlayListUrl,
                                               PlayListType = Playlist.PlayListType,
                                               TrackTimeCount = Playlist.TrackDuration,
                                               IsLocked = UserplayListDetail.IsLocked ?? false,
                                               Favourite = UserplayListDetail.IsFav,
                                               TextTrackshortDescription = Playlist.Description.Substring(0, 150),
                                               TextTrackLongDescription = Playlist.Description,
                                           }).ToList(),

                          }).ToList();
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, Result, Request);
            }
            catch (Exception ex)
            {

                return obj.APIResponse(HttpStatusCode.OK, true, ex.Message.ToString(), TextTrackList, Request);
            }




        }


        [HttpGet]
        public HttpResponseMessage GetTextTrackwithAudioTrack(long UserId, int ProgramId)
        {
            int TypeId = 2;
            int TakeRecord = 0;
            var request = HttpContext.Current.Request;
            string baseUrl = request.Url.Scheme + "://" + request.Url.Authority + '/';
            try
            {
                // get text track 
                List<TrackListViewModel> TextTrackList = (from Playlist in db.tbl_ProgramPlayList
                                                          join UserplayListDetail in db.tbl_UsersProgPlayListDetail on
                                                          Playlist.pkProgramPlayListId equals UserplayListDetail.fkProgramPlayListId
                                                          where Playlist.fkProgramId == ProgramId && UserplayListDetail.UserId == UserId && Playlist.PlayListType == 1
                                                           && ((Playlist.IsBonusTrack == null ? false : Playlist.IsBonusTrack) == false)
                                                          select new TrackListViewModel()
                                                          {
                                                              ProgramId = Playlist.fkProgramId,
                                                              AudioTrackId = Playlist.pkProgramPlayListId,
                                                              TrackName = Playlist.PlayList,
                                                              TrackArtist = Playlist.TrackArtist,
                                                              PlayListType = Playlist.PlayListType,
                                                              TrackTimeCount = Playlist.TrackDuration,
                                                              IsLocked = UserplayListDetail.IsLocked ?? false,
                                                              Favourite = UserplayListDetail.IsFav,
                                                              TextTrackshortDescription = Playlist.Description.Substring(0, 150),
                                                              TextTrackLongDescription = Playlist.Description,
                                                              ValidDate = UserplayListDetail.ValidDate

                                                          }).ToList();

                TakeRecord = TextTrackList.Count();
                DateTime DateCourseOpen = TextTrackList[0].ValidDate;

                //var ProgramDate =  (from x in db.tbl_UsersProgPlayListDetail where x.fkProgramId == ProgramId select new {CreatedDate = x.ValidDate }).Take(1);
                var ProgramDate1 = db.tbl_UsersProgPlayListDetail.Where(X => X.fkProgramId == ProgramId && X.UserId == UserId).Select(y => new { CreatedDate = y.ValidDate }).FirstOrDefault();

                List<ProgramTrackViewModel> Result = new List<ProgramTrackViewModel>();
                Result = (from ProMaster in db.tbl_ProgramMaster
                          join UserProgmDetail in db.tbl_UsersProgrameDetails on  ProMaster.pkProgramId equals UserProgmDetail.fkProgramId
                          where UserProgmDetail.fkProgramId == ProgramId && UserProgmDetail.UserId == UserId && UserProgmDetail.IsCourseOpen == true

                          select new ProgramTrackViewModel()
                          {
                              CourseName = ProMaster.Name,
                              CourseImage = "",
                              SessionDay = ProMaster.Session,
                              CreatedDate = DateCourseOpen,
                              TrackList = (from Playlist in db.tbl_ProgramPlayList
                                           join UserplayListDetail in db.tbl_UsersProgPlayListDetail on
                                           Playlist.pkProgramPlayListId equals UserplayListDetail.fkProgramPlayListId
                                           where
                                        Playlist.fkProgramId == ProgramId && UserplayListDetail.UserId == UserId && Playlist.PlayListType == TypeId && Playlist.fkProgramId == ProgramId
                                        && ((Playlist.IsBonusTrack == null ? false : Playlist.IsBonusTrack) == false)
                                           select new TrackListViewModel()
                                           {
                                               ProgramId = Playlist.fkProgramId,
                                               AudioTrackId = Playlist.pkProgramPlayListId,
                                               // TrackName = Playlist.PlayList,
                                               AudioTrackName = Playlist.PlayList,
                                               TrackArtist = Playlist.TrackArtist,
                                               TrackUrl = baseUrl + "Upload/" + Playlist.PlayListUrl,
                                               PlayListType = Playlist.PlayListType,
                                               TrackTimeCount = Playlist.TrackDuration,
                                               IsLocked = UserplayListDetail.IsLocked ?? false,
                                               Favourite = UserplayListDetail.IsFav,
                                               TrackDescription = Playlist.Description,
                                               ShortDescription = Playlist.Description.Substring(0, 150),

                                           }).Take(TakeRecord).ToList(),

                          }).ToList();
                int i = 0;
                foreach (var y in TextTrackList)
                {
                    if (Result[0].TrackList.Count > i + 1 || Result[0].TrackList.Count == i + 1)
                    {
                        Result[0].TrackList[i].TextTrackshortDescription = y.TextTrackshortDescription.ToString();
                        Result[0].TrackList[i].TextTrackLongDescription = y.TextTrackLongDescription.ToString();
                        Result[0].TrackList[i].TrackName = y.TrackName;
                        i++;
                    }
                    else
                    {
                        TrackListViewModel item = new TrackListViewModel();
                        item.ProgramId = y.ProgramId;
                        item.TrackName = y.TrackName;
                        item.TrackUrl = "";
                        item.TrackTimeCount = "";
                        item.TextTrackshortDescription = y.TextTrackshortDescription;
                        item.TextTrackLongDescription = y.TextTrackLongDescription;
                        Result[0].TrackList.Add(item);
                    }
                }

                //int i = 0;
                //foreach (var y in TextTrackList)
                //{
                //    if (Result[0].TrackList.Count > i + 1 || Result[0].TrackList.Count == i + 1)
                //    {
                //        Result[0].TrackList[i].TextTrackshortDescription = TextTrackList[i].TextTrackshortDescription.ToString();
                //        Result[0].TrackList[i].TextTrackLongDescription = TextTrackList[i].TextTrackshortDescription.ToString();
                //    }
                //    else
                //    {
                //        TrackListViewModel item = new TrackListViewModel();
                //        item.ProgramId = y.ProgramId;
                //        item.TrackName = y.TrackName;
                //        item.TextTrackshortDescription = y.TextTrackshortDescription;
                //        item.TextTrackLongDescription = y.TextTrackLongDescription;
                //        Result[0].TrackList.Add(item);
                //    }


                //    i++;
                //}


                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, Result, Request);
            }
            catch (Exception ex)
            {
                CommonMessage.Error = ex.Message.ToString();
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Error, "", Request);
            }
            finally
            {

            }


        }


        [HttpGet]
        public HttpResponseMessage GetTextTrackwithAudioTrackBackEndCal(long UserId, int ProgramId)
        {
            int TypeId = 2;
            int TakeRecord = 0;
            var request = HttpContext.Current.Request;
            string baseUrl = request.Url.Scheme + "://" + request.Url.Authority + '/';
            try
            {
                // get text track 
                List<TrackListViewModel> TextTrackList = (from Playlist in db.tbl_ProgramPlayList
                                                          join UserplayListDetail in db.tbl_UsersProgPlayListDetail on
                                                          Playlist.pkProgramPlayListId equals UserplayListDetail.fkProgramPlayListId
                                                          where Playlist.fkProgramId == ProgramId
                                                          && UserplayListDetail.UserId == UserId 
                                                          && Playlist.PlayListType == 1
                                                          && ((Playlist.IsBonusTrack == null ? false : Playlist.IsBonusTrack) == false)
                                                          select new TrackListViewModel()
                                                          {
                                                              ProgramId = Playlist.fkProgramId,
                                                              AudioTrackId = Playlist.pkProgramPlayListId,
                                                              TrackName = Playlist.PlayList,
                                                              TrackArtist = Playlist.TrackArtist,
                                                              PlayListType = Playlist.PlayListType,
                                                              TrackTimeCount = Playlist.TrackDuration,
                                                              IsLocked = UserplayListDetail.IsLocked ?? false,
                                                              Favourite = UserplayListDetail.IsFav,
                                                              TextTrackshortDescription = Playlist.Description.Substring(0, 150),
                                                              TextTrackLongDescription = Playlist.Description,
                                                              ValidDate = UserplayListDetail.ValidDate

                                                          }).ToList();

                TakeRecord = TextTrackList.Count();
               
             
                //var ProgramDate =  (from x in db.tbl_UsersProgPlayListDetail where x.fkProgramId == ProgramId select new {CreatedDate = x.ValidDate }).Take(1);
                var ProgramDate1 = db.tbl_UsersProgPlayListDetail.Where(X => X.fkProgramId == ProgramId && X.UserId == UserId).Select(y => new { CreatedDate = y.ValidDate }).FirstOrDefault();

                List<ProgramTrackViewModel> Result = new List<ProgramTrackViewModel>();
                Result = (from ProMaster in db.tbl_ProgramMaster
                          join UserProgmDetail in db.tbl_UsersProgrameDetails on ProMaster.pkProgramId equals UserProgmDetail.fkProgramId
                          where UserProgmDetail.fkProgramId == ProgramId && UserProgmDetail.UserId == UserId && UserProgmDetail.IsCourseOpen == true

                          select new ProgramTrackViewModel()
                          {
                              CourseName = ProMaster.Name,
                              CourseImage = "",
                              SessionDay = ProMaster.Session,
                              CreatedDate = ProgramDate1.CreatedDate,
                              TrackList = (from Playlist in db.tbl_ProgramPlayList
                                           join UserplayListDetail in db.tbl_UsersProgPlayListDetail on
                                           Playlist.pkProgramPlayListId equals UserplayListDetail.fkProgramPlayListId
                                           where
                                        Playlist.fkProgramId == ProgramId 
                                        && UserplayListDetail.UserId == UserId 
                                        && Playlist.PlayListType == TypeId 
                                        && Playlist.fkProgramId == ProgramId
                                        //&& ((Playlist.IsBonusTrack == null ? false : Playlist.IsBonusTrack) == false)
                                           select new TrackListViewModel()
                                           {
                                               ProgramId = Playlist.fkProgramId,
                                               AudioTrackId = Playlist.pkProgramPlayListId,
                                               // TrackName = Playlist.PlayList,
                                               AudioTrackName = Playlist.PlayList,
                                               TrackArtist = Playlist.TrackArtist,
                                               TrackUrl = baseUrl + "Upload/" + Playlist.PlayListUrl,
                                               PlayListType = Playlist.PlayListType,
                                               TrackTimeCount = Playlist.TrackDuration,
                                               IsLocked = UserplayListDetail.IsLocked ?? false,
                                               Favourite = UserplayListDetail.IsFav,
                                               TrackDescription = Playlist.Description,
                                               ShortDescription = Playlist.Description.Substring(0, 150),

                                           }).Take(TakeRecord).ToList(),

                          }).ToList();
                int i = 0;
                foreach (var y in TextTrackList)
                {
                    if (Result[0].TrackList.Count > i + 1 || Result[0].TrackList.Count == i + 1)
                    {
                        Result[0].TrackList[i].TextTrackshortDescription = y.TextTrackshortDescription.ToString();
                        Result[0].TrackList[i].TextTrackLongDescription = y.TextTrackLongDescription.ToString();
                        Result[0].TrackList[i].TrackName = y.TrackName;
                        i++;
                    }
                    else
                    {
                        TrackListViewModel item = new TrackListViewModel();
                        item.ProgramId = y.ProgramId;
                        item.TrackName = y.TrackName;
                        item.TrackUrl = "";
                        item.TrackTimeCount = "";
                        item.TextTrackshortDescription = y.TextTrackshortDescription;
                        item.TextTrackLongDescription = y.TextTrackLongDescription;
                        Result[0].TrackList.Add(item);
                    }
                }

                //int i = 0;
                //foreach (var y in TextTrackList)
                //{
                //    if (Result[0].TrackList.Count > i + 1 || Result[0].TrackList.Count == i + 1)
                //    {
                //        Result[0].TrackList[i].TextTrackshortDescription = TextTrackList[i].TextTrackshortDescription.ToString();
                //        Result[0].TrackList[i].TextTrackLongDescription = TextTrackList[i].TextTrackshortDescription.ToString();
                //    }
                //    else
                //    {
                //        TrackListViewModel item = new TrackListViewModel();
                //        item.ProgramId = y.ProgramId;
                //        item.TrackName = y.TrackName;
                //        item.TextTrackshortDescription = y.TextTrackshortDescription;
                //        item.TextTrackLongDescription = y.TextTrackLongDescription;
                //        Result[0].TrackList.Add(item);
                //    }


                //    i++;
                //}
                //    DateTime currentdate = DateTime;


                //  Local side
                //string TrackValidCreatedDate = Result[0].CreatedDate.HasValue ? Result[0].CreatedDate.Value.ToString("dd/MM/yyyy") : DateTime.Now.Date.ToString("dd/MM/yyyy");
                //string currentDate = DateTime.Now.ToString("dd/MM/yyyy");
                //double FirstdayTrackOpen = (Convert.ToDateTime(currentDate) - Convert.ToDateTime(TrackValidCreatedDate)).TotalDays;
                //  Local side

                // //  server side
                string TrackValidCreatedDate = Result[0].CreatedDate.HasValue ? Result[0].CreatedDate.Value.ToString("M/d/yyyy") : DateTime.Now.Date.ToString("M/d/yyyy");
                string currentDate = DateTime.Now.ToString("M/d/yyyy");
                double FirstdayTrackOpen = (Convert.ToDateTime(currentDate) - Convert.ToDateTime(TrackValidCreatedDate)).TotalDays;


                for (int l = 0; l < Result[0].TrackList.Count();l++)
                {

                    if(l < FirstdayTrackOpen)
                    {
                        Result[0].TrackList[l].TrackTimePeriod = "Past";
              
                    }
                    if (l == FirstdayTrackOpen)
                    {
                        Result[0].TrackList[l].TrackTimePeriod = "Today";
                     

                    }
                   else if ((l - FirstdayTrackOpen) == 1)
                    {

                        if (string.IsNullOrEmpty(Result[0].TrackList[l].TrackTimePeriod))
                        {
                            Result[0].TrackList[l].TrackTimePeriod = "Tomorrow";
                       
                        }
                    }
                   else if ((l + 1) > FirstdayTrackOpen)
                    {
                        if (string.IsNullOrEmpty(Result[0].TrackList[l].TrackTimePeriod))
                        {
                            Result[0].TrackList[l].TrackTimePeriod = "Upcomming";
                      
                        }
                    }


                }
                          





                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, Result, Request);
            }
            catch (Exception ex)
            {
                CommonMessage.Error = ex.Message.ToString();
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Error, "", Request);
            }
            finally
            {

            }


        }



        [HttpGet]
        public HttpResponseMessage GetAllAudioTrackPlayList(long UserId)
        {
            int TypeId = 2;
            var request = HttpContext.Current.Request;
            string baseUrl = request.Url.Scheme + "://" + request.Url.Authority + '/';

            try
            {
                List<ProgramTrackViewModel> Result = (from ProMaster in db.tbl_ProgramMaster
                                                      join UserProgmDetail in db.tbl_UsersProgrameDetails on
                                                            ProMaster.pkProgramId equals UserProgmDetail.fkProgramId
                                                      where UserProgmDetail.UserId == UserId && UserProgmDetail.IsCourseOpen == true
                                                      select new ProgramTrackViewModel()
                                                      {
                                                          CourseName = ProMaster.Name,
                                                          CourseImage = ProMaster.ImageUrl,
                                                          SessionDay = ProMaster.Session,
                                                          CreatedDate = UserProgmDetail.CreatedDate,
                                                          TrackList = (from Playlist in db.tbl_ProgramPlayList
                                                                       join UserplayListDetail in db.tbl_UsersProgPlayListDetail on
                                                                       Playlist.pkProgramPlayListId equals UserplayListDetail.fkProgramPlayListId
                                                                       where Playlist.fkProgramId == ProMaster.pkProgramId && UserplayListDetail.UserId == UserId
                                                                       && Playlist.PlayListType == TypeId
                                                                       select new TrackListViewModel()
                                                                       {
                                                                           ProgramId = ProMaster.pkProgramId,
                                                                           AudioTrackId = Playlist.pkProgramPlayListId,
                                                                           TrackName = Playlist.PlayList,
                                                                           TrackArtist = Playlist.TrackArtist,
                                                                           TrackUrl = baseUrl + "Upload/" + Playlist.PlayListUrl,
                                                                           PlayListType = Playlist.PlayListType,
                                                                           TrackTimeCount = Playlist.TrackDuration,
                                                                           IsLocked = UserplayListDetail.IsLocked ?? false,
                                                                           Favourite = UserplayListDetail.IsFav,
                                                                           TrackDescription = Playlist.Description
                                                                       }).ToList(),

                                                      }).ToList();


                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, Result, Request);



            }
            catch (Exception ex)
            {
                CommonMessage.Error = ex.Message.ToString();
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Error, "", Request);
            }
            finally
            {

            }

        }


        [HttpGet]
        public HttpResponseMessage GetAllAudioUnlockTrackPlayList(long UserId)
        {
            int TypeId = 2;
            var request = HttpContext.Current.Request;
            string baseUrl = request.Url.Scheme + "://" + request.Url.Authority + '/';
            List<TrackListViewModel> Result2 = new List<TrackListViewModel>();
            try
            {
                List<ProgramTrackViewModel> Result = (from ProMaster in db.tbl_ProgramMaster
                                                      join UserProgmDetail in db.tbl_UsersProgrameDetails on
                                                            ProMaster.pkProgramId equals UserProgmDetail.fkProgramId
                                                      where UserProgmDetail.UserId == UserId && UserProgmDetail.IsCourseOpen == true
                                                      select new ProgramTrackViewModel()
                                                      {

                                                          TrackList = (from Playlist in db.tbl_ProgramPlayList
                                                                       join UserplayListDetail in db.tbl_UsersProgPlayListDetail on
                                                                       Playlist.pkProgramPlayListId equals UserplayListDetail.fkProgramPlayListId
                                                                       where Playlist.fkProgramId == ProMaster.pkProgramId && UserplayListDetail.UserId == UserId
                                                                       && UserplayListDetail.IsLocked == false
                                                                       && Playlist.PlayListType == TypeId
                                                                       select new TrackListViewModel()
                                                                       {
                                                                           ProgramId = Playlist.fkProgramId,
                                                                           AudioTrackId = Playlist.pkProgramPlayListId,
                                                                           TrackName = Playlist.PlayList,
                                                                           TrackArtist = Playlist.TrackArtist,
                                                                           TrackDescription = Playlist.Description,
                                                                           TrackUrl = baseUrl + "Upload/" + Playlist.PlayListUrl,
                                                                           PlayListType = Playlist.PlayListType,
                                                                           TrackTimeCount = Playlist.TrackDuration,
                                                                           IsLocked = UserplayListDetail.IsLocked ?? false,
                                                                           Favourite = UserplayListDetail.IsFav,

                                                                       }).ToList(),

                                                      }).ToList();

                foreach (ProgramTrackViewModel x in Result)
                {
                    foreach (TrackListViewModel y in x.TrackList)
                    {
                        Result2.Add(y);
                    }
                }

                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, Result2, Request);



            }
            catch (Exception ex)
            {
                CommonMessage.Error = ex.Message.ToString();
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Error, "", Request);
            }
            finally
            {

            }

        }

        /// <summary>
        /// ////////////
        /// </summary>
        // Bonuse track
        /// <returns></returns>

        public HttpResponseMessage GetBonusTrack(long UserId, int ProgramId)
        {
            int TypeId = 2;
            var request = HttpContext.Current.Request;
            string baseUrl = request.Url.Scheme + "://" + request.Url.Authority + '/';
            try
            {
                var Reslult1 = db.tbl_UsersProgPlayListDetail.Where(x => x.UserId == UserId && x.IsLocked == true).FirstOrDefault();
                DateTime modifiedDatetime = DateTime.Now;
                double hourdiff = (modifiedDatetime - Reslult1.ValidDate).TotalHours;
                if (hourdiff > 24)
                {
                    //  db.Proc_UnlockTrackforUser(UserId);
                }

                List<ProgramTrackViewModel> Result = (from ProMaster in db.tbl_ProgramMaster
                                                      join UserProgmDetail in db.tbl_UsersProgrameDetails on
                                                            ProMaster.pkProgramId equals UserProgmDetail.fkProgramId
                                                      where UserProgmDetail.fkProgramId == ProgramId && UserProgmDetail.UserId == UserId && UserProgmDetail.IsCourseOpen == true

                                                      select new ProgramTrackViewModel()
                                                      {
                                                          CourseName = ProMaster.Name,
                                                          CourseImage = "",
                                                          SessionDay = ProMaster.Session,
                                                          CreatedDate = ProMaster.CreatedDate,

                                                          TrackList = (from Playlist in db.tbl_ProgramPlayList
                                                                       join UserplayListDetail in db.tbl_UsersProgPlayListDetail on
                                                                       Playlist.pkProgramPlayListId equals UserplayListDetail.fkProgramPlayListId
                                                                       where
                                                                    Playlist.fkProgramId == ProgramId && UserplayListDetail.UserId == UserId && Playlist.PlayListType == TypeId
                                                                    && ((Playlist.IsBonusTrack == null ? false : Playlist.IsBonusTrack) == true)
                                                                       select new TrackListViewModel()
                                                                       {
                                                                           ProgramId = Playlist.fkProgramId,
                                                                           AudioTrackId = Playlist.pkProgramPlayListId,
                                                                           TrackName = Playlist.PlayList,
                                                                           TrackArtist = Playlist.TrackArtist,
                                                                           TrackUrl = baseUrl + "Upload/" + Playlist.PlayListUrl,
                                                                           PlayListType = Playlist.PlayListType,
                                                                           TrackTimeCount = Playlist.TrackDuration,
                                                                           IsLocked = UserplayListDetail.IsLocked ?? false,
                                                                           Favourite = UserplayListDetail.IsFav,
                                                                           TrackDescription = Playlist.Description,

                                                                       }).ToList(),

                                                      }).ToList();
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, Result, Request);
            }
            catch (Exception ex)
            {
                CommonMessage.Error = ex.Message.ToString();
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Error, "", Request);
            }
            finally
            {

            }
        }
        


       [HttpGet]
        public HttpResponseMessage GetAllBonusTrackList(long UserId, int ProgramId)
        {
            int TypeId = 2;
            var request = HttpContext.Current.Request;
            string baseUrl = request.Url.Scheme + "://" + request.Url.Authority + '/';
            List<TrackListViewModel> Result2 = new List<TrackListViewModel>();
            try
            {
                List<ProgramTrackViewModel> Result = (from ProMaster in db.tbl_ProgramMaster
                                                      join UserProgmDetail in db.tbl_UsersProgrameDetails on
                                                            ProMaster.pkProgramId equals UserProgmDetail.fkProgramId
                                                      where ProMaster.pkProgramId == ProgramId && UserProgmDetail.UserId == UserId && UserProgmDetail.IsCourseOpen == true 
                                                      select new ProgramTrackViewModel()
                                                      {

                                                          CourseName = ProMaster.Name,
                                                          CourseImage = ProMaster.ImageUrl,
                                                          SessionDay = ProMaster.Session,
                                                          CreatedDate = UserProgmDetail.CreatedDate,
                                                          TrackList = (from Playlist in db.tbl_ProgramPlayList
                                                                       join UserplayListDetail in db.tbl_UsersProgPlayListDetail on
                                                                       Playlist.pkProgramPlayListId equals UserplayListDetail.fkProgramPlayListId
                                                                       where Playlist.fkProgramId == ProMaster.pkProgramId && UserplayListDetail.UserId == UserId
                                                                       && Playlist.PlayListType == TypeId && ((Playlist.IsBonusTrack == null ? false : Playlist.IsBonusTrack) == true)
                                                                       select new TrackListViewModel()
                                                                       {
                                                                           ProgramId = Playlist.fkProgramId,
                                                                           AudioTrackId = Playlist.pkProgramPlayListId,
                                                                           TrackName = Playlist.PlayList,
                                                                           TrackArtist = Playlist.TrackArtist,
                                                                           TrackUrl = baseUrl + "Upload/" + Playlist.PlayListUrl,
                                                                           PlayListType = Playlist.PlayListType,
                                                                           TrackTimeCount = Playlist.TrackDuration,
                                                                           IsLocked = UserplayListDetail.IsLocked ?? false,
                                                                           Favourite = UserplayListDetail.IsFav,
                                                                           TrackDescription = Playlist.Description,
                                                                       }).ToList(),

                                                      }).ToList();

                //foreach (ProgramTrackViewModel x in Result)
                //{
                //    foreach (TrackListViewModel y in x.TrackList)
                //    {
                //        Result2.Add(y);
                //    }
                //}

                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, Result, Request);



            }
            catch (Exception ex)
            {
                CommonMessage.Error = ex.Message.ToString();
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Error, "", Request);
            }
            finally
            {

            }

        }


        [HttpGet]
        public HttpResponseMessage ProgramCategory(long UserId)
        {
            var request = HttpContext.Current.Request;
            string baseUrl = request.Url.Scheme + "://" + request.Url.Authority + '/';
            try
            {

            List<ProgramTrackViewModel> Result = new List<ProgramTrackViewModel>();
            Result = (from ProMaster in db.tbl_ProgramMaster
                      join UserProgmDetail in db.tbl_UsersProgrameDetails on
                            ProMaster.pkProgramId equals UserProgmDetail.fkProgramId
                      where UserProgmDetail.UserId == UserId 

                      select new ProgramTrackViewModel()
                      {
                          
                          CourseName = ProMaster.Name,
                          CourseImage = UserProgmDetail.IsCourseOpen == true ? baseUrl + "Upload/ProgramImages/ActiveProgram" +  ProMaster.Name + "/" + ProMaster.ImageUrl : baseUrl + "Upload/ProgramImages/InActiveProgram" + ProMaster.Name + "/" + ProMaster.ImageUrl,
                          SessionDay = ProMaster.Session,
                          ProgramId = ProMaster.pkProgramId,
                          CreatedDate = UserProgmDetail.CreatedDate,
                          IsCourseOpen = UserProgmDetail.IsCourseOpen,

                      }).ToList();

            return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, Result, Request);
            }
            catch (Exception ex)
            {

                return obj.APIResponse(HttpStatusCode.OK, true, ex.Message.ToString(), "", Request);
            }

        }

        [HttpPost]
        public  HttpResponseMessage AddCourseCompletionRecord(CourseCompletinModel Model)
        {
            try
            {

                tbl_UsersProgPlayListDetail Objtbl_UsersProgPlayListDetail = db.tbl_UsersProgPlayListDetail.Where(x => x.UserId == Model.UserId && x.fkProgramPlayListId == Model.TrackId).FirstOrDefault();

                tbl_ProgramPlayList Objtbl_PlayListTrackOrder = db.tbl_ProgramPlayList.Where(x => x.pkProgramPlayListId == Model.TrackId).FirstOrDefault();
                string TempTrackDuration = "";
                if (Objtbl_PlayListTrackOrder != null)
                {
                    TempTrackDuration =  Objtbl_PlayListTrackOrder.TrackDuration;
                }
                if (Model.TrackDuration == TempTrackDuration)
                {
                    Objtbl_UsersProgPlayListDetail.IsTrackListen = true;
                    db.Entry(Objtbl_UsersProgPlayListDetail).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, "", Request);
            }
            catch (Exception ex)
            {

                return obj.APIResponse(HttpStatusCode.OK, true, ex.Message.ToString(), "", Request);
            }
        }


        [HttpPost]
        public HttpResponseMessage GetCourseCompletionRecord(CourseCompletionCalculateModel Model)
        {
            try
            {

               CourseCompletionCalculateModel Obj_temp_tbl_UsersProgPlayListDetail= new CourseCompletionCalculateModel();
               List<CourseCompletionCalculateModel> Objtbl_UsersProgPlayListDetail = (from Temptbl_ProgramPlayList in db.tbl_ProgramPlayList
                                                                                    join TempUserplayListDetail in db.tbl_UsersProgPlayListDetail on Temptbl_ProgramPlayList.pkProgramPlayListId equals TempUserplayListDetail.fkProgramPlayListId
                                                                                    where TempUserplayListDetail.fkProgramId == Model.ProgramId
                                                                                    && TempUserplayListDetail.UserId == Model.UserId 
                                                                                    && Temptbl_ProgramPlayList.PlayListType == 2
                                                                                    && Temptbl_ProgramPlayList.IsBonusTrack == false
                                                                                    select new CourseCompletionCalculateModel()
                                                                                    {
                                                                                        CompletionPercentage = 0,
                                                                                        TrackId = Temptbl_ProgramPlayList.pkProgramPlayListId,
                                                                                        IsListen = TempUserplayListDetail.IsTrackListen ?? false
                                                                                    }).ToList();
                int TotalTrackCount = Objtbl_UsersProgPlayListDetail.Count();
                int TrackListend = 0;
                if (TotalTrackCount != 0)
                {
                     TrackListend = Objtbl_UsersProgPlayListDetail.Where(x => x.IsListen == true).Count();
                     Obj_temp_tbl_UsersProgPlayListDetail.CompletionPercentage = (TrackListend * 100 / TotalTrackCount);
                }
                Obj_temp_tbl_UsersProgPlayListDetail.UserId = Model.UserId;
                Obj_temp_tbl_UsersProgPlayListDetail.UserId = Model.ProgramId;

                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, Obj_temp_tbl_UsersProgPlayListDetail, Request);
            }
            catch (Exception ex)
            {

                return obj.APIResponse(HttpStatusCode.OK, true, ex.Message.ToString(), "", Request);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
