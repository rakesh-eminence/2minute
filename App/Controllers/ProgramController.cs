﻿using App.Common;
using App.Filters;
using App.Models;
using App.viewmodel;
using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace App.Controllers
{
    [BasicAuthentication]
    public class ProgramController : ApiController
    {
        private m1TestEntities db = new m1TestEntities();
        string Host = ConfigurationManager.AppSettings["Host"].ToString();
        CommonHelper obj = new CommonHelper();
        [HttpGet]
        public HttpResponseMessage GetPrograms()
        {
            try
            {
                var res = db.tbl_ProgramMaster.Select(x => new { x.Name, x.Description, x.Session });
                return obj.APIResponse(HttpStatusCode.OK, true, res, "", Request);
            }
            catch (Exception ex)
            {
                return obj.APIResponse(HttpStatusCode.OK, false, ex.Message, "", Request);
            }
        }


        /// <summary>
        /// calling this API for home page
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetProgramsByUser(int userId)
        {
            try
            {
             

                List<ProgramMasterViewModel> objProgramMasterViewModel = new List<ProgramMasterViewModel>();

                var IsExistUser = db.tbl_UsersProgrameDetails.Where(x => x.UserId == userId).ToList();
                var dataprogramFirstOopen = db.tbl_UsersProgPlayListDetail.Where(x => x.UserId == userId).FirstOrDefault();
                long ? programIdFirstopen = 0;
                if (dataprogramFirstOopen != null)
                {
                    programIdFirstopen = dataprogramFirstOopen.fkProgramId;
                }
        
                string Host = ConfigurationManager.AppSettings["Host"].ToString();

                if (IsExistUser.Count > 0)
                {
                    objProgramMasterViewModel = (from c in db.tbl_ProgramMaster
                                                 join p in db.tbl_UsersProgrameDetails on c.pkProgramId equals p.fkProgramId into ps
                                                 from p in ps.DefaultIfEmpty()
                                                 select new ProgramMasterViewModel()
                                                 {
                                                     pkProgramId = c.pkProgramId,
                                                     Name = c.Name,
                                                     Price = c.Price,
                                                     Description = c.Description,
                                                     IsActive = p.IsActive,
                                                     IsCourseOpen = p.IsCourseOpen,
                                                     PaymentText = c.PaymentText,
                                                     //  ImageUrl = Host + (p.IsActive ? "Upload/ProgramImages/ActiveProgram/" : "Upload/ProgramImages/InActiveProgram/") + c.ImageUrl.ToString(),
                                                     ImageUrl =  c.IsProgramUnlockForAllUser.ToString() ==  "true" ? Host + "Upload/ProgramImages/ActiveProgram/" + c.ImageUrl.ToString() :  (p.IsActive ? Host + "Upload/ProgramImages/ActiveProgram/" + c.ImageUrl.ToString() : Host + "Upload/ProgramImages/InActiveProgram/" + c.ImageUrl.ToString()) ,
                                                     ValidDate = p.ValidDate,
                                                     UserId = p.UserId,
                                                     IsProgramUnlockForAllUser = (c.pkProgramId == programIdFirstopen) ? true : c.IsProgramUnlockForAllUser ?? false ,
                                                     //ImageUrl = Host + (c.IsProgramUnlockForAllUser ?? false ? "Upload/ProgramImages/ActiveProgram/" : "Upload/ProgramImages/InActiveProgram/") + c.ImageUrl.ToString(),
                                                 }).Where(p => p.UserId == userId).ToList();
                    // when program purchased then show all course
                   var stripeResp = (from stripPay in db.tbl_StripePaymentDetail select new { stripPay.ProgramId, stripPay.UserId }).Where(p => p.UserId == userId).ToList();

                    foreach (var item in objProgramMasterViewModel)
                    {
                         if(stripeResp.Where(x => x.ProgramId == item.pkProgramId).ToList().Count() > 0)
                        {
                            item.IsProgramUnlockForAllUser = true;


                        }
                        else
                        {
                            item.IsProgramUnlockForAllUser = item.IsProgramUnlockForAllUser;
             
                        }

                    }




                }
                else
                {
                    objProgramMasterViewModel = db.tbl_ProgramMaster.Select(x => new ProgramMasterViewModel()
                    {
                      pkProgramId=  x.pkProgramId,
                        Name =  x.Name,
                        Price = x.Price,
                        Description =   x.Description,
                        Session = x.Session,
                        IsActive = x.IsActive,
                        IsCourseOpen = false,
                        ImageUrl = x.IsProgramUnlockForAllUser.ToString() == "true" ? Host + "Upload/ProgramImages/ActiveProgram/" + x.ImageUrl.ToString() : x.IsProgramUnlockForAllUser.ToString() == "false" ? Host + "Upload/ProgramImages/InActiveProgram/" + x.ImageUrl.ToString() : (x.IsActive.ToString() == "true" ? Host + "Upload/ProgramImages/ActiveProgram/" + x.ImageUrl.ToString() : Host + "Upload/ProgramImages/InActiveProgram/" + x.ImageUrl.ToString()),
                        PaymentText = x.PaymentText,
                        ValidDate = DateTime.Now,
                        UserId = userId,
                        IsProgramUnlockForAllUser = x.IsProgramUnlockForAllUser ?? false,
                    }).ToList();

                }
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, objProgramMasterViewModel, Request);
            }
            catch (Exception ex)
            {
                return obj.APIResponse(HttpStatusCode.OK, false, ex.Message, "", Request);
            }
        }

        [HttpGet]
        public HttpResponseMessage GetProgramsDetails(long Id, long? TrackId)
        {
            try
            {
                if (TrackId == 0)
                {
                    var res = db.tbl_ProgramMaster.Where(x => x.pkProgramId == Id).Select(x => new
                    {
                        x.pkProgramId,
                        x.Name,
                        ShortDescription = x.Description.Substring(0, 150),
                        LongDescription = x.Description,
                        x.Session,
                        ProgramMessage = ""
                    }).FirstOrDefault();

                    return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, res, Request);
                }
                else
                {
                    var res = (from playlist in db.tbl_ProgramPlayList
                               join program in db.tbl_ProgramMaster
                               on playlist.fkProgramId equals program.pkProgramId
                               where playlist.pkProgramPlayListId == TrackId && playlist.PlayListType == 1
                               select new
                               {
                                   pkProgramId = program.pkProgramId,
                                   Name = program.Name,
                                   ShortDescription = program.Description.Substring(0, 150),
                                   LongDescription = program.Description,
                                   Session = program.Session,
                                   ProgramMessage = playlist.Description
                               }).FirstOrDefault();
                    return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, res, Request);

                }

            }
            catch (Exception ex)
            {
                return obj.APIResponse(HttpStatusCode.OK, false, ex.Message, "", Request);
            }
        }
        [HttpGet]
        public HttpResponseMessage GetTextPlayList()
        {
            try
            {
                //PlayListType=1 means only for text
                var res = from playlist in db.tbl_ProgramPlayList
                          join program in db.tbl_ProgramMaster
                          on playlist.fkProgramId equals program.pkProgramId
                          where playlist.PlayListType == 1
                          select new { program.Name, playlist.PlayList };

                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, res, Request);

            }
            catch (Exception ex)
            {
                return obj.APIResponse(HttpStatusCode.OK, false, ex.Message, "", Request);
            }
        }
        [HttpGet]
        public HttpResponseMessage GetAudioPlayList()
        {
            try
            {
                //PlayListType=2 means only for text
                //var res = from playlist in db.tbl_ProgramPlayList
                //          join program in db.tbl_ProgramMaster
                //          on playlist.fkProgramId equals program.pkProgramId
                //          where playlist.PlayListType == 2
                //          select new { playlist.pkProgramPlayListId, program.Name, playlist.PlayList };
                var res = from playlist in db.tbl_ProgramPlayList
                          join program in db.tbl_ProgramMaster
                          on playlist.fkProgramId equals program.pkProgramId
                          join userplaylst in db.tbl_UsersProgPlayListDetail
                          on playlist.pkProgramPlayListId equals userplaylst.fkProgramPlayListId into gj
                          from user in gj.DefaultIfEmpty()
                          where playlist.PlayListType == 2
                          select new { playlist.pkProgramPlayListId, program.Name, playlist.PlayList, user.IsFav };

                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, res, Request);

            }
            catch (Exception ex)
            {
                return obj.APIResponse(HttpStatusCode.OK, false, ex.Message, "", Request);
            }
        }



        /// <summary>
        /// Calling this API to unlock user program
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="programId"></param>
        /// <returns></returns> 
        [HttpPost]
        public HttpResponseMessage UnlockUserProgram(int userID, int programId)
        {
            try
            {

                string Response = string.Empty;
                db.Proc_SaveUsersProgram(userID, programId);
                var res = (from c in db.tbl_ProgramMaster
                           join p in db.tbl_UsersProgrameDetails on c.pkProgramId equals p.fkProgramId into ps
                           from p in ps.DefaultIfEmpty()
                           select new
                           {
                               c.Name,
                               c.Description,
                               p.IsActive,
                               ImageUrl = Host + "Upload/ProgramImages/" + c.ImageUrl.ToString(),
                               p.ValidDate,
                               p.UserId,
                               IsProgramUnlockForAllUser = c.pkProgramId == programId ? true : false,
                           }).Where(p => p.UserId == userID).ToList();

                db.Proc_SaveTrackforNewUser(programId, userID, 2);// insert default track for new user
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.UnlockCourse, res, Request);


            }
            catch (Exception)
            {
                return obj.APIResponse(HttpStatusCode.OK, false, CommonMessage.ExceptionMsg, "", Request);
            }
        }

        //******************************************************  SURVEY  ****************************************************
        [HttpGet]
        public HttpResponseMessage GetSurvey(int userId)
        {
            try
            {
                List<clsSurvey> clsSurvey = new List<clsSurvey>();

                clsSurvey = db.tbl_ProgramMaster.Where(x => x.IsActive == true).Select(x => new clsSurvey { pkProgramId = x.pkProgramId, Name = x.Name }).ToList();

                if (clsSurvey != null)
                {
                    var user = db.tbl_UserSurvey.Where(x => x.UserId == userId).ToList();
                    if (user.Count > 0)
                    {
                        foreach (var sur in clsSurvey)
                        {
                            var survey = (from c in db.tbl_SurveyMaster
                                          join p in db.tbl_UserSurvey on c.pkSurveyMasterId equals p.fkSurveyMasterId into ps
                                          from p in ps.DefaultIfEmpty()
                                          select new SurveyList
                                          {
                                              Percentage = p.Percentage,
                                              Questions = c.Questions,
                                              ProgramId = c.fkProgramId,
                                              UserId = p.UserId,
                                              pkSurveyMasterId = c.pkSurveyMasterId
                                          }).Where(p => p.ProgramId == sur.pkProgramId && p.UserId == userId).ToList();
                            sur.SurveyList = survey;
                        }
                    }
                    else
                    {
                        foreach (var sur in clsSurvey)
                        {
                            var survey = (from c in db.tbl_SurveyMaster
                                          select new SurveyList
                                          {
                                              Percentage = 0,
                                              Questions = c.Questions,
                                              ProgramId = c.fkProgramId,
                                              UserId = userId,
                                              pkSurveyMasterId = c.pkSurveyMasterId
                                          }).Where(p => p.ProgramId == sur.pkProgramId && p.UserId == userId).ToList();
                            sur.SurveyList = survey;
                        }
                    }

                }

                return obj.APIResponse(HttpStatusCode.OK, true, clsSurvey, "", Request);
            }
            catch (Exception ex)
            {
                return obj.APIResponse(HttpStatusCode.OK, false, ex.Message, "", Request);
            }
        }
        [HttpPost]
        public HttpResponseMessage PostUserSurvey(UserSurvey userSurvey)
        {
            try
            {
                string Response = string.Empty;
                List<tbl_UserSurvey> std = db.tbl_UserSurvey.Where(i => i.UserId == userSurvey.UserId).ToList();
                if (std.Count > 0)
                {
                    foreach (var s in userSurvey.SurveyList)
                    {
                        var UserSurvey = db.tbl_UserSurvey.First(i => i.UserId == s.UserId && i.fkSurveyMasterId == s.pkSurveyMasterId);
                        UserSurvey.Percentage = s.Percentage;
                        UserSurvey.UserId = s.UserId;
                        UserSurvey.fkProgramId = s.fkProgramId;
                        UserSurvey.fkSurveyMasterId = s.pkSurveyMasterId;
                        db.SaveChanges();
                    }
                }
                else
                {
                    foreach (var s in userSurvey.SurveyList)
                    {
                        tbl_UserSurvey tbl_UserSurvey = new tbl_UserSurvey();
                        //var UserSurvey = db.tbl_UserSurvey.First(i => i.UserId == s.UserId && i.fkSurveyMasterId == s.pkSurveyMasterId);
                        tbl_UserSurvey.Percentage = s.Percentage;
                        tbl_UserSurvey.UserId = s.UserId;
                        tbl_UserSurvey.fkProgramId = s.fkProgramId;
                        tbl_UserSurvey.fkSurveyMasterId = s.pkSurveyMasterId;
                        db.tbl_UserSurvey.Add(tbl_UserSurvey);
                        db.SaveChanges();
                    }
                }

                //int userId = Convert.ToInt32(userSurvey.UserId);
                //var res = GetSurvey(userId);
                //var data = res.Content.ReadAsStringAsync().Result;
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, "", Request);

            }
            catch (Exception)
            {
                return obj.APIResponse(HttpStatusCode.OK, false, CommonMessage.ExceptionMsg, "", Request);
            }
        }

        [HttpGet]
        public HttpResponseMessage GetUserScoreBoard(int userId)
        {
            try
            {
                List<ScoreBoard> ScoreBoard = new List<ScoreBoard>();
                string ConnectionString = CommonHelper.GetConnectionString();
                //IDbConnection conn = new SqlConnection(ConnectionString);
                using (IDbConnection db = new SqlConnection(ConnectionString))
                {
                    string readSp = "RecommendSurvey";
                    ScoreBoard = db.Query<ScoreBoard>(readSp, new { @UserId = userId }, commandType: CommandType.StoredProcedure).ToList();
                }
                foreach (var s in ScoreBoard)
                {
                    s.ImageUrl = Host + "Upload/ProgramImages/ActiveProgram/" + s.ImageUrl.ToString();
                }
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, ScoreBoard, Request);
            }
            catch (Exception)
            {
                return obj.APIResponse(HttpStatusCode.OK, false, CommonMessage.ExceptionMsg, "", Request);
            }
        }


        [HttpPost]
        public HttpResponseMessage AddFav(int userID, int playListId)
        {
            try
            {
                string Response = string.Empty;
                var std = db.tbl_UsersProgPlayListDetail.First(i => i.fkProgramPlayListId == playListId && i.UserId == userID);
                std.IsFav = true;
                db.SaveChanges();
                var res = from playlist in db.tbl_ProgramPlayList
                          join program in db.tbl_ProgramMaster
                          on playlist.fkProgramId equals program.pkProgramId
                          join userplaylst in db.tbl_UsersProgPlayListDetail
                          on playlist.pkProgramPlayListId equals userplaylst.fkProgramPlayListId
                          where playlist.PlayListType == 2
                          select new { playlist.pkProgramPlayListId, program.Name, playlist.PlayList, userplaylst.IsFav };
                return obj.APIResponse(HttpStatusCode.OK, true, res, "", Request);

            }
            catch (Exception)
            {
                return obj.APIResponse(HttpStatusCode.OK, false, CommonMessage.ExceptionMsg, "", Request);
            }
        }


        [HttpGet]
        public HttpResponseMessage GetCourseCompletionData(int userId)
        {
            try
            {

                return obj.APIResponse(HttpStatusCode.OK, true, "", "", Request);

            }
            catch (Exception)
            {

                return obj.APIResponse(HttpStatusCode.OK, false, CommonMessage.ExceptionMsg, "", Request);
            }
        }

        //********************************* Notification ********************************************

        [HttpGet]
        public HttpResponseMessage GetUerNotification(int userId)
        {
            try
            {
                var res = db.tbl_Notification.Select(x => new { x.Text, x.CreatedDate, x.IsRead });
                return obj.APIResponse(HttpStatusCode.OK, true, res, "", Request);
            }
            catch (Exception ex)
            {
                return obj.APIResponse(HttpStatusCode.OK, false, ex.Message, "", Request);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
