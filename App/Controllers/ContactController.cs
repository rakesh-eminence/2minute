﻿using App.Common;
using App.Models;
using App.viewmodel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;


namespace App.Controllers
{
    public class ContactController : ApiController
    {
        private m1TestEntities db;
        string Host = ConfigurationManager.AppSettings["Host"].ToString();
        CommonHelper obj = new CommonHelper();

        [HttpPost]
        public HttpResponseMessage AddcontactusRecord(ContactusViewModel ViewModel)
        {
      
            tbl_Contactus Objtbl_Contactus = new tbl_Contactus();
            try
            {
                using (var db = new m1TestEntities())
                {
                    Objtbl_Contactus.UserId = ViewModel.UserId;
                    Objtbl_Contactus.Title = ViewModel.Title;
                    Objtbl_Contactus.Message = ViewModel.Message;
                    db.tbl_Contactus.Add(Objtbl_Contactus);
                    db.SaveChanges();


                    tbl_LoginMaster Objtbl_LoginMaster = db.tbl_LoginMaster.Where(x => x.IsAdmin == true).FirstOrDefault();
                    tbl_LoginMaster res = db.tbl_LoginMaster.Where(x => x.pkId == ViewModel.UserId).FirstOrDefault();
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Hi " + "Admin" + ",");
                    sb.Append("<br/>");
                    sb.Append(ViewModel.Message);
                    sb.Append("<br/>");
                    sb.Append("<br/>");
                    sb.Append("Thanks,");
                    sb.Append("<br/>");
                    sb.Append(ViewModel.Name +",");
                    sb.Append("<br/>");
                    sb.Append(ViewModel.Email + ",");
                    sb.Append("<br/>");
                    sb.Append("2 Minute Brain");
                    EmailHelper.SendEmail(Objtbl_LoginMaster.EmailId, "", "", ViewModel.Title, sb.ToString());
                  

                    
                }
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.ContactSave, "", Request);
            }
            catch (Exception ex)
            {

                return obj.APIResponse(HttpStatusCode.OK, false, CommonMessage.ExceptionMsg, "", Request);
            }
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
