﻿    using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using App.Filters;
using App.Models;
using App.viewmodel;

namespace App.Controllers
{
    //[Authorize, SessionExpire]
    public class ProgramsController : Controller
    {
        private m1TestEntities db = new m1TestEntities();

        // GET: ProgramMaster
        public ActionResult Index(long? id)
        {
            List<ProgramViewModel> ObjProgramList = new List<ProgramViewModel>();
            try
            {

        
            ObjProgramList = (from c in db.tbl_ProgramMaster
                    
                       select new ProgramViewModel()
                       {
                           pkProgramId = c.pkProgramId,
                           ImageUrl = c.ImageUrl,
                           Name = c.Name,
                           Session = c.Session,
                           AudioTrackCount = (from pl in db.tbl_ProgramPlayList where pl.fkProgramId == c.pkProgramId && pl.PlayListType == 2 && ((pl.IsBonusTrack == null ? false : pl.IsBonusTrack) == false) select pl).Count(),
                           TextCount = (from pl in db.tbl_ProgramPlayList where pl.fkProgramId == c.pkProgramId && pl.PlayListType ==1 && ((pl.IsBonusTrack == null ? false : pl.IsBonusTrack) == false) select pl).Count(),
                           BonusTrack = (from pl in db.tbl_ProgramPlayList where pl.fkProgramId == c.pkProgramId && pl.IsBonusTrack == true select pl).Count(),
                           IsProgramUnlockForAllUser = c.IsProgramUnlockForAllUser ?? false
                       }).ToList();
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();


            }
            return View(ObjProgramList);
            // return View(db.tbl_ProgramMaster.ToList());
        }

        // GET: ProgramMaster/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_ProgramMaster tbl_ProgramMaster = db.tbl_ProgramMaster.Find(id);
            if (tbl_ProgramMaster == null)
            {
                return HttpNotFound();
            }
            return View(tbl_ProgramMaster);
        }

        // GET: ProgramMaster/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ProgramMaster/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken, ValidateInput(false)]
        public ActionResult Create( tbl_ProgramMaster tbl_ProgramMaster, HttpPostedFileBase file)
        {
            string fileName = string.Empty;
            string destinationPath = string.Empty;
            //if (ModelState.IsValid)
            //{
            //    db.tbl_ProgramMaster.Add(tbl_ProgramMaster);
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}
            if (ModelState.IsValid)
            {
                HttpPostedFileBase file_Uploader = Request.Files[0] as HttpPostedFileBase;
                if (file_Uploader.ContentLength > 0)
                {

                    fileName = file_Uploader.FileName;
                    destinationPath = Path.Combine(Server.MapPath("~/Upload/ProgramImages/ActiveProgram/"), fileName);
                    file_Uploader.SaveAs(destinationPath);
                    tbl_ProgramMaster.ImageUrl = fileName;


                }
                else
                {

                    tbl_ProgramMaster.ImageUrl = "defaultProgram.png";

                }
                tbl_ProgramMaster.CreatedDate = DateTime.Now;
                tbl_ProgramMaster.CreatedBy = "ADMIN";
                db.tbl_ProgramMaster.Add(tbl_ProgramMaster);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tbl_ProgramMaster);
        }

        // GET: ProgramMaster/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_ProgramMaster tbl_ProgramMaster = db.tbl_ProgramMaster.Find(id);
            if (tbl_ProgramMaster == null)
            {
                return HttpNotFound();
            }
            return View(tbl_ProgramMaster);
        }

        // POST: ProgramMaster/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken, ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "pkProgramId,Name,Description,ImageUrl,Session")] tbl_ProgramMaster tbl_ProgramMaster)
        {
            string fileName = string.Empty;
            string destinationPath = string.Empty;
            tbl_ProgramMaster Result = db.tbl_ProgramMaster.Where(x => x.pkProgramId == tbl_ProgramMaster.pkProgramId).FirstOrDefault();
            if (ModelState.IsValid)
            {

                HttpPostedFileBase file_Uploader = Request.Files[0] as HttpPostedFileBase;
                if (file_Uploader.ContentLength > 0)
                {
                    if (!Result.ImageUrl.Contains("defaultProgram"))
                    {
                        int pos = Result.ImageUrl.LastIndexOf("/") + 1;
                        fileName = Result.ImageUrl.Substring(pos, Result.ImageUrl.Length - pos);
                        destinationPath = Path.Combine(Server.MapPath("~/Upload/ProgramImages/ActiveProgram/"), fileName);

                        if (System.IO.File.Exists(destinationPath))
                        {
                            System.IO.File.Delete(destinationPath);
                        }

                    }



                    fileName = file_Uploader.FileName;
                    destinationPath = Path.Combine(Server.MapPath("~/Upload/ProgramImages/ActiveProgram/"), fileName);
                    file_Uploader.SaveAs(destinationPath);
                    Result.Name = tbl_ProgramMaster.Name;
                    Result.Description = tbl_ProgramMaster.Description.Replace("&nbsp;", " ");
                    Result.ImageUrl = fileName;
                    Result.Session = tbl_ProgramMaster.Session;
                    

                }
                else
                {
                    Result.Name = tbl_ProgramMaster.Name;
                    Result.Description = tbl_ProgramMaster.Description.Replace("&nbsp;", " ");
                    Result.ImageUrl = Result.ImageUrl;
                    Result.Session = tbl_ProgramMaster.Session;
                 
           

                }
                Result.CreatedDate = DateTime.Now;
                db.Entry(Result).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tbl_ProgramMaster);
        }




        public ActionResult GetPaymentData()
        {

            List<ProgramViewModel> ObjProgramList = new List<ProgramViewModel>();
            try
            {


                ObjProgramList = (from c in db.tbl_ProgramMaster

                                  select new ProgramViewModel()
                                  {
                                      pkProgramId = c.pkProgramId,
                                      ImageUrl = c.ImageUrl,
                                      Name = c.Name,
                                      Price = c.Price,
                                      PaymentText = c.PaymentText,
                                  }).ToList();
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();


            }
            return View(ObjProgramList);

        }

        public ActionResult EditPaymentData(long? id)
        {
            tbl_ProgramMaster ObjProgramList = new tbl_ProgramMaster();
            try
            {
                ObjProgramList = db.tbl_ProgramMaster.Find(id);

            }
            catch (Exception ex)
            {

                string exception = ex.ToString();
            }
            return View(ObjProgramList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken, ValidateInput(false)]
        public ActionResult CreateProgramEdit(tbl_ProgramMaster tbl_ProgramMaster)
        {
            if (ModelState.IsValid)
            {
                tbl_ProgramMaster Result = db.tbl_ProgramMaster.Where(x => x.pkProgramId == tbl_ProgramMaster.pkProgramId).FirstOrDefault();

                Result.PaymentText = tbl_ProgramMaster.PaymentText;
                Result.Price = tbl_ProgramMaster.Price;
                Result.Name = tbl_ProgramMaster.Name;
                db.Entry(Result).State = EntityState.Modified;
                db.SaveChanges();
            }

            return RedirectToAction("GetPaymentData");
        }


        // GET: ProgramMaster/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tbl_ProgramMaster tbl_ProgramMaster = db.tbl_ProgramMaster.Find(id);
            if (tbl_ProgramMaster == null)
            {
                return HttpNotFound();
            }
            return View(tbl_ProgramMaster);
        }

        // POST: ProgramMaster/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            tbl_ProgramMaster tbl_ProgramMaster = db.tbl_ProgramMaster.Find(id);
            db.tbl_ProgramMaster.Remove(tbl_ProgramMaster);
          //  db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult ActiveInactiveProgram(ProgramMasterViewModel Model)
        {
            tbl_ProgramMaster Result = db.tbl_ProgramMaster.Where(x => x.pkProgramId == Model.pkProgramId).FirstOrDefault();
            try
            {
                Result.IsProgramUnlockForAllUser = Model.IsProgramUnlockForAllUser;
                db.Entry(Result).State = EntityState.Modified;
                db.SaveChanges();
               ;
           
            }
            catch (Exception ex)
            {
                string ex1 = ex.Message.ToString();
                
            }
            return RedirectToAction("Index");

        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
