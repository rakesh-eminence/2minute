﻿using App.Filters;
using App.Models;
using App.viewmodel;
using Microsoft.WindowsAPICodePack.Shell;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace App.Controllers
{
    [Authorize, SessionExpire]
    public class BonusTrackAdminController : Controller
    {
        private m1TestEntities db = new m1TestEntities();
        string Host = ConfigurationManager.AppSettings["Host"].ToString();
        // GET: BonusTrackAdmin
        public ActionResult Audiotrackviewlist(long? id)
        {

            List<ProgramPlayListViewModel> ObjProgramPlayList = new List<ProgramPlayListViewModel>();
            using (db = new m1TestEntities())
            {
                ObjProgramPlayList = (from c in db.tbl_ProgramPlayList
                                      where c.fkProgramId == id && c.PlayListType == 2 && c.IsBonusTrack == true
                                      select new ProgramPlayListViewModel()
                                      {
                                          pkProgramPlayListId = c.pkProgramPlayListId,
                                          PlayList = c.PlayList,
                                          CreatedBy = c.CreatedBy,
                                          Description = c.Description,
                                          TrackArtist = c.TrackArtist,
                                          TrackUrl = c.PlayListUrl,
                                          TrackTimeCount = c.TrackDuration,
                                      }).ToList();


            }


            TempData["ProgramId"] = id;

            return View(ObjProgramPlayList);
        }

        public ActionResult Create()
        {
            if (TempData["ProgramId"] != null)
            {
                int ProgramId = Convert.ToInt32(TempData["ProgramId"]);
                ViewBag.ProgramId = ProgramId;
                TempData["ProgramId"] = ProgramId;
            }
            else
            {
                return RedirectToAction("Index", "Programs");
            }
            return View();
        }



        [HttpPost]
        [ValidateAntiForgeryToken, ValidateInput(false)]
        public ActionResult Create(ProgramPlayListViewModel tbl_ProgramPlayListModel, HttpPostedFileBase file)
        {

            long ProgramId = 0;
            tbl_ProgramPlayList Objtbl_ProgramPlayList = new tbl_ProgramPlayList();

            string fileName = string.Empty;
            string guidwithFile = string.Empty;
            string destinationPath = string.Empty;
            string strpath = string.Empty;
            try
            {
                using (var db = new m1TestEntities())
                {
                    ProgramId = int.Parse(TempData["ProgramId"].ToString());
                    var Result = db.tbl_ProgramMaster.Where(x => x.pkProgramId == ProgramId).FirstOrDefault();
                    if (ModelState.IsValid)
                    {
                        HttpPostedFileBase file_Uploader = Request.Files[0] as HttpPostedFileBase;
                        if (file_Uploader.ContentLength > 0)
                        {

                            fileName = Path.GetFileName(file_Uploader.FileName).Replace("_", "-");
                            guidwithFile = Guid.NewGuid().ToString().Substring(0, 20).Replace("_", "-");
                            strpath = System.IO.Path.GetExtension(file_Uploader.FileName);
                            guidwithFile = guidwithFile + strpath; ;

                            bool exists = System.IO.Directory.Exists(Path.Combine(Server.MapPath("~/Upload/" + Result.Name)));
                            if (!exists)
                            {
                                System.IO.Directory.CreateDirectory(Path.Combine(Server.MapPath("~/Upload/" + Result.Name)));
                            }
                            destinationPath = Path.Combine(Server.MapPath("~/Upload/" + Result.Name + "/"), guidwithFile);
                            file_Uploader.SaveAs(destinationPath);


                        }
                        string tracktime = string.Empty;
                        string[] arr = new string[] { ".png", ".jpg", ".jpeg", ".gif", ".bmp", ".webp" };
                     if (!arr.Contains(strpath))
                        {
                            using (var shell = ShellObject.FromParsingName(destinationPath))
                            {
                                IShellProperty prop = shell.Properties.System.Media.Duration;
                                var t = (ulong)prop.ValueAsObject;
                                tracktime = TimeSpan.FromTicks((long)t).ToString();
                            }
                        }

                        Objtbl_ProgramPlayList.fkProgramId = ProgramId;
                        Objtbl_ProgramPlayList.PlayList = string.IsNullOrEmpty(tbl_ProgramPlayListModel.PlayList) ? fileName : tbl_ProgramPlayListModel.PlayList;
                        Objtbl_ProgramPlayList.PlayListUrl = Result.Name + "/" + guidwithFile;
                        Objtbl_ProgramPlayList.PlayListType = 2;
                        Objtbl_ProgramPlayList.CreatedDate = DateTime.UtcNow;
                        Objtbl_ProgramPlayList.CreatedBy = "ADMIN";
                        Objtbl_ProgramPlayList.TrackArtist = tbl_ProgramPlayListModel.TrackArtist;
                        Objtbl_ProgramPlayList.Description = tbl_ProgramPlayListModel.Description.Replace("&nbsp;", " "); 
                        Objtbl_ProgramPlayList.TrackDuration = tracktime.Split('.')[0].ToString();
                        Objtbl_ProgramPlayList.IsBonusTrack = true;
                        db.tbl_ProgramPlayList.Add(Objtbl_ProgramPlayList);
                        db.SaveChanges();
                        long pkProgramPlayListId = Objtbl_ProgramPlayList.pkProgramPlayListId;
                        var res = db.Proc_SaveNewTrackforUser(ProgramId, pkProgramPlayListId, 2);
                        return RedirectToAction("Audiotrackviewlist", new { id = ProgramId });
                    }
                }
            }
            catch (Exception ex)
            {
                string test = ex.Message.ToString();

            }
            TempData["ProgramId"] = ProgramId;
            return RedirectToAction("Audioindex", new { id = ProgramId });
            //   return View(tbl_ProgramPlayListModel);
        }


        public ActionResult Editbonusaudiotrack(long? id)
        {

            if (TempData["ProgramId"] != null)
            {
                int ProgramId = Convert.ToInt32(TempData["ProgramId"]);
                ViewBag.ProgramId = ProgramId;
                TempData["ProgramId"] = ProgramId;
            }
            else
            {
                return RedirectToAction("Index", "Programs");
            }
            tbl_ProgramPlayList Objtbl_ProgramPlayList = new tbl_ProgramPlayList();
            try
            {


                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Objtbl_ProgramPlayList = db.tbl_ProgramPlayList.Find(id);
                if (Objtbl_ProgramPlayList == null)
                {
                    return HttpNotFound();
                }
            }
            catch (Exception ex)
            {
                string x = ex.Message.ToString();
                throw;
            }

            return View(Objtbl_ProgramPlayList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken, ValidateInput(false)]
        public ActionResult Editbonusaudiotrack(tbl_ProgramPlayList tbl_ProgramPlayListModel)
        {
            string fileName = string.Empty;
            string destinationPath = string.Empty;
            string oldDestination = string.Empty;
            string newdestinationPath = string.Empty;
            tbl_ProgramPlayList Result = db.tbl_ProgramPlayList.Where(x => x.pkProgramPlayListId == tbl_ProgramPlayListModel.pkProgramPlayListId).FirstOrDefault();
            if (ModelState.IsValid)
            {


                HttpPostedFileBase file_Uploader = Request.Files[0] as HttpPostedFileBase;
                if (file_Uploader.ContentLength > 0)
                {
                    destinationPath = Path.Combine(Server.MapPath("~/Upload/")); // base path
                    oldDestination = Path.Combine(Server.MapPath("~/Upload/")); //old base path

                    fileName = Path.GetFileName(file_Uploader.FileName).Replace("_", "-");
                    string guidwithFile = Guid.NewGuid().ToString().Substring(0, 20).Replace("_", "-");
                    string strpath = System.IO.Path.GetExtension(file_Uploader.FileName);
                    guidwithFile = guidwithFile + strpath;
                    if (!Result.PlayListUrl.Contains("defaultProgram"))
                    {
                        var OldFilename = new String[4];

                        if (Result.PlayListUrl.Contains('/'))
                        {
                            OldFilename = Result.PlayListUrl.Split('/');
                        }
                        newdestinationPath = Path.Combine(Server.MapPath("~/Upload/" + OldFilename[0].ToString() + "/" + guidwithFile));
                        oldDestination = Path.Combine(Server.MapPath("~/Upload/" + OldFilename[0].ToString() + "/" + OldFilename[1]));
                        Result.PlayListUrl = OldFilename[0].ToString() + "/" + guidwithFile;
                        Result.PlayList = tbl_ProgramPlayListModel.PlayList;

                        if (System.IO.File.Exists(oldDestination))
                        {
                            System.IO.File.SetAttributes(oldDestination, FileAttributes.Normal);
                            System.IO.File.Delete(oldDestination);
                        }

                        file_Uploader.SaveAs(newdestinationPath);

                    }
                    using (var db = new m1TestEntities())
                    {

                        ////////////////////////
                        string tracktime = string.Empty;
                        string[] arr = new string[] { ".png", ".jpg", ".jpeg", ".gif", ".bmp", ".webp" };
                        if (!arr.Contains(strpath))
                        {
                            
                            using (var shell = ShellObject.FromParsingName(newdestinationPath))
                            {
                                IShellProperty prop = shell.Properties.System.Media.Duration;
                                var t = (ulong)prop.ValueAsObject;
                                tracktime = TimeSpan.FromTicks((long)t).ToString();
                            }

                        }
                        /////////////////
                        ///

                        Result.TrackArtist = tbl_ProgramPlayListModel.TrackArtist;
                        Result.Description = tbl_ProgramPlayListModel.Description.Replace("&nbsp;", " "); 
                        Result.CreatedDate = DateTime.Now;
                        Result.TrackDuration = tracktime.Split('.')[0].ToString();
                        Result.IsBonusTrack = true;
                        db.Entry(Result).State = EntityState.Modified;
                        db.SaveChanges();

                    }
                    return RedirectToAction("Audiotrackviewlist", new { id = Result.fkProgramId });

                }
                else
                {

        

                    using (var db = new m1TestEntities())
                    {
                        Result.PlayList = tbl_ProgramPlayListModel.PlayList;
                        Result.TrackArtist = tbl_ProgramPlayListModel.TrackArtist;
                        Result.Description = tbl_ProgramPlayListModel.Description.Replace("&nbsp;", " "); ;
                        Result.CreatedDate = DateTime.Now;
                        db.Entry(Result).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                }

                return RedirectToAction("Audiotrackviewlist", new { id = Result.fkProgramId });
            }

            return View("Editaudiotrack");

        }

        // GET: BonusTrackAdmin/Delete/5
        public ActionResult Delete(int? id)
        {
            ProgramPlayListViewModel ObjProgramPlayLis = new ProgramPlayListViewModel();
            using (db = new m1TestEntities())
            {
                ObjProgramPlayLis = (from c in db.tbl_ProgramPlayList
                                     where c.pkProgramPlayListId == id && c.PlayListType == 2 && c.IsBonusTrack == true
                                     select new ProgramPlayListViewModel()
                                     {
                                         pkProgramPlayListId = c.pkProgramPlayListId,
                                         PlayList = c.PlayList,
                                         CreatedBy = c.CreatedBy,
                                         CreatedDate = c.CreatedDate,
                                         Description = c.Description,
                                         TrackArtist = c.TrackArtist,
                                         //   TrackUrl = c.PlayListUrl,
                                         TrackTimeCount = c.TrackDuration,
                                         fkProgramId = c.fkProgramId,
                                     }).FirstOrDefault();

            }


            TempData["ProgramId"] = ObjProgramPlayLis.fkProgramId;
            ViewBag.pkProgramPlayListId = id;
            return View(ObjProgramPlayLis);
        }
        [HttpPost]
        public ActionResult delete(int id, FormCollection collection) 
        {

            try
            {
                

                ProgramPlayListViewModel ObjProgramPlayLis = new ProgramPlayListViewModel();
                using (db = new m1TestEntities())
                {
                    tbl_ProgramPlayList Objtbl_ProgramPlayList = new tbl_ProgramPlayList();

                         Objtbl_ProgramPlayList = db.tbl_ProgramPlayList.Find(id);
                        var OldFilename = new String[4];

                        if (Objtbl_ProgramPlayList.PlayListUrl.Contains('/'))
                        {
                            OldFilename = Objtbl_ProgramPlayList.PlayListUrl.Split('/');
                        }
                        
                       string  oldDestination = Path.Combine(Server.MapPath("~/Upload/" + OldFilename[0].ToString() + "/" + OldFilename[1]));
                        if (System.IO.File.Exists(oldDestination))
                        {
                            System.IO.File.SetAttributes(oldDestination, FileAttributes.Normal);
                            System.IO.File.Delete(oldDestination); //  Delete file before remove from database
                        }


                    if (Objtbl_ProgramPlayList != null)
                    {
                        db.tbl_ProgramPlayList.Remove(Objtbl_ProgramPlayList);
                        db.SaveChanges();

                        List<tbl_UsersProgPlayListDetail> Objtbl_UsersProgPlayListDetail = new List<tbl_UsersProgPlayListDetail>();
                        Objtbl_UsersProgPlayListDetail = db.tbl_UsersProgPlayListDetail.Where(x => x.fkProgramPlayListId == id).ToList();

                        foreach (var item in Objtbl_UsersProgPlayListDetail)
                        {
                            var Result = db.tbl_UsersProgPlayListDetail.Find(item.pkUserPGPlayListId);
                            db.tbl_UsersProgPlayListDetail.Remove(Result);
                            db.SaveChanges();
                        }
                        return RedirectToAction("Audioindex", new { id = Objtbl_ProgramPlayList.fkProgramId });
                    }
                    else
                    {
                        return RedirectToAction("Audioindex", new { id = 0 });
                    }

                }
            }
            catch (Exception)
            {


            }


            return RedirectToAction("Index", "Programs");

            //TempData["ProgramId"] = model.fkProgramId;

        }

        // POST: BonusTrackAdmin/Delete/5

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
