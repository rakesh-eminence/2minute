﻿using App.Common;
using App.Filters;
using App.Models;
using App.viewmodel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace App.Controllers
{
    [BasicAuthentication]
    public class QueueController : ApiController
    {
        private m1TestEntities db = new m1TestEntities();
        string Host = ConfigurationManager.AppSettings["Host"].ToString();
        CommonHelper obj = new CommonHelper();

        [HttpPost]
        public HttpResponseMessage CreateTrackQueue(long TrackId, long UserId)
        {

            try
            {
                using (var db = new m1TestEntities())
                {

                    var Result = db.tbl_UsersProgPlayListDetail.Where(x => x.pkUserPGPlayListId == TrackId).FirstOrDefault();


                    Result.IsAddedinQueue = true;
                    db.Entry(Result).State = EntityState.Modified;
                    db.SaveChanges();
                    return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, "", Request);
                }
            }
            catch (Exception)
            {
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, "", Request);

            }
        }

        public HttpResponseMessage GetAllAudioTrackQueueList(long UserId)
        {
            int TypeId = 2;
            var request = HttpContext.Current.Request;
            string baseUrl = request.Url.Scheme + "://" + request.Url.Authority + '/';

            try
            {
                List<ProgramTrackViewModel> Result = (from ProMaster in db.tbl_ProgramMaster
                                                      join UserProgmDetail in db.tbl_UsersProgrameDetails on
                                                            ProMaster.pkProgramId equals UserProgmDetail.fkProgramId
                                                      where UserProgmDetail.UserId == UserId && UserProgmDetail.IsCourseOpen == true
                                                      select new ProgramTrackViewModel()
                                                      {
                                                          CourseName = ProMaster.Name,
                                                          CourseImage = ProMaster.ImageUrl,
                                                          SessionDay = ProMaster.Session,
                                                          CreatedDate = UserProgmDetail.CreatedDate,
                                                          TrackList = (from Playlist in db.tbl_ProgramPlayList
                                                                       join UserplayListDetail in db.tbl_UsersProgPlayListDetail on
                                                                       Playlist.pkProgramPlayListId equals UserplayListDetail.fkProgramPlayListId
                                                                       where Playlist.fkProgramId == ProMaster.pkProgramId && UserplayListDetail.UserId == UserId
                                                                       && Playlist.PlayListType == TypeId && UserplayListDetail.IsAddedinQueue == true
                                                                       select new TrackListViewModel()
                                                                       {
                                                                           ProgramId = ProMaster.pkProgramId,
                                                                           AudioTrackId = Playlist.pkProgramPlayListId,
                                                                           TrackName = Playlist.PlayList,
                                                                           TrackArtist = Playlist.TrackArtist,
                                                                           TrackUrl = baseUrl + "Upload/" + Playlist.PlayListUrl,
                                                                           PlayListType = Playlist.PlayListType,
                                                                           TrackTimeCount = Playlist.TrackDuration,
                                                                           IsLocked = UserplayListDetail.IsLocked ?? false,
                                                                           Favourite = UserplayListDetail.IsFav,
                                                                           TrackDescription = Playlist.Description,
                                                                           IsQuequTrack = UserplayListDetail.IsAddedinQueue 
                                                                       }).ToList(),

                                                      }).ToList();


                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Success, Result, Request);



            }
            catch (Exception ex)
            {
                CommonMessage.Error = ex.Message.ToString();
                return obj.APIResponse(HttpStatusCode.OK, true, CommonMessage.Error, "", Request);
            }
            finally
            {

            }

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
