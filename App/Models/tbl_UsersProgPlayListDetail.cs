//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace App.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_UsersProgPlayListDetail
    {
        public long pkUserPGPlayListId { get; set; }
        public int PlayListType { get; set; }
        public long fkProgramPlayListId { get; set; }
        public bool IsActive { get; set; }
        public long UserId { get; set; }
        public Nullable<long> fkProgramId { get; set; }
        public System.DateTime ValidDate { get; set; }
        public bool IsFav { get; set; }
        public Nullable<bool> IsLocked { get; set; }
        public Nullable<long> fkCustomUserPlayListId { get; set; }
        public Nullable<bool> IsAddedinQueue { get; set; }
        public Nullable<bool> IsTrackListen { get; set; }
    }
}
