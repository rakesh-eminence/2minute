﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.viewmodel
{
    public class NotificationViewModel
    {
       public long NotificationId { get; set; }

        public string Name { get; set; }

       public long? ProgramId { get; set; }

        public long UserId { get; set; }

        public string DeviceType { get; set; }

        public string DeviceToken { get; set; }



    }
}