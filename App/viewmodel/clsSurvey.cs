﻿using App.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.viewmodel
{
    public class clsSurvey
    {
        public long pkProgramId { get; set; }
        public string Name { get; set; }

        public List<SurveyList> SurveyList { get; set; }


    }
    public class SurveyList
    {
        public long pkSurveyMasterId { get; set; }
        public string Questions { get; set; }
        public decimal Percentage { get; set; }
        public long ProgramId { get; set; }
        public long UserId { get; set; }
        public long fkProgramId { get; set; }
        //public System.DateTime CreatedDate { get; set; }
        //public string CreatedBy { get; set; }
    }
    public class UserSurvey
    {
        public long UserId { get; set; }
        public List<SurveyList> SurveyList { get; set; }
    }

    public class ScoreBoard
    {
        public long pkProgramId { get; set; }
        public string Name { get; set; }
        public string  ImageUrl { get; set; }
        public decimal Percentage { get; set; }
        public bool IsRecommended { get; set; }
    }
}