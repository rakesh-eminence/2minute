﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace App.viewmodel
{
    public class PlayLIstViewModel
    {

        [Required(ErrorMessage = "Please enter the name.")]
        public string PlayList { get; set; }




    }

    public class TrackListViewModel
    {

        public long ProgramId { get; set; }
        public long AudioTrackId { get; set; }
        public string TrackUrl { get; set; }
        public int PlayListType { get; set; }

        public bool IsLocked { get; set; }

        public bool Favourite { get; set; }

        public string TrackTimeCount { get; set; }

        public string TrackArtist { get; set; }

        public string TrackName { get; set; }

        public string AudioTrackName { get; set; }

        public string TrackDescription { get; set; }

        public bool? IsQuequTrack { get; set; }

        public string ShortDescription { get; set; }


        public string TextTrackshortDescription { get; set; }

        public string TextTrackLongDescription { get; set; }

        public string TrackTimePeriod { get; set; }
        public DateTime ValidDate { get; set; }
    }


    public class ProgramTrackViewModel
    {
        public string CourseName { get; set; }

        public string CourseImage { get; set; }

        public int SessionDay { get; set; }

        public DateTime? CreatedDate { get; set; }

        public long ProgramId { get; set; }

        public bool IsCourseOpen { get; set; }



        public List<TrackListViewModel> TrackList { get; set; }
    }


    public class UserPlayListViewModel
    {


        public string PlayListName { get; set; }
        public long PlayListId { get; set; }
        public long UserId { get; set; }
        public List<UserPlayListViewModel> UserPlayList { get; set; }
        public List<PlayListTrackViewModel> PlayTrackList { get; set; }
        public int? OrderByPlayList { get; set; }
        public int Index { get; set; }

    }

    public class PlayListTrackViewModel
    {
        public long PlayLIstTrackId { get; set; }
        public bool IsFav { get; set; }
    }

    public class UserCustomPlayListandTrackViewModel
    {


        public long ProgramId { get; set; }
        public long AudioTrackId { get; set; }
        public string TrackUrl { get; set; }
        public int PlayListType { get; set; }

        public bool IsLocked { get; set; }

        public bool Favourite { get; set; }

        public string TrackTimeCount { get; set; }

        public string TrackArtist { get; set; }

        public string TrackName { get; set; }

        public string TrackDescription { get; set; }

        public string PlayListName { get; set; }

        public long PlayListId { get; set; }



        public int? OrderByIndex { get; set; }


    }


    public class TrackOrderModel
    {

        public long UserId { get; set; }

        public int PlayListId { get; set; }

        public List<TrackModel> TrackModelList { get; set; }



    }

    public class TrackModel
    {
        public int TrackId { get; set; }

        public int Index { get; set; }

    }


    public class PopularTrackProgramDtoModel
    {

        public int ProgramId { get; set; }

        public int TrackId { get; set; }

        public int UserId { get; set; }

    }



    public class APPUserDtoModel
    {

        public int UserId { get; set; }

        public bool IsAppOpen { get; set; }


    }


}