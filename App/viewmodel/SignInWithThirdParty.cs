﻿using App.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace App.viewmodel
{
    public class SignInWithThirdParty
    {

        [Required]
        public ThirdPartyType ThirdPartyType { get; set; }



        public string Id { get; set; }
        public string Name { get; set; }
        public string EmailId { get; set; }
        public string Password { get; set; }
        public string ImageUrl { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime Date { get; set; }
        public string DeviceToken { get; set; }
        public string DeviceType { get; set; }
        public string FacebookId { get; set; }

    }
    public class ImageResponse
    {
        public string ThumbnailURL { get; set; }
        public string ImageURL { get; set; }

        public string BannerImage_URL { get; set; }

        public bool IsSuccess { get; set; }
        public string ResponseMessage { get; set; }

        public string GuidImage { get; set; }
    }

    public class ChnagePassWordviewModel
    {
        public long UserId { get; set; }
        public string OldPassword { get; set; }

        public string NewPassword { get; set; }
    }
}