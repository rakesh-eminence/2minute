﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.viewmodel
{
    public class ProgramModel
    {




    }

    public class ProgramViewModel
    {

        public long pkProgramId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public int Session { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public int AudioTrackCount { get; set; }

        public int TextCount { get; set; }
        public int BonusTrack { get; set; }

        public bool IsProgramUnlockForAllUser { get; set; }

        public  string PaymentText { get; set; }
        public decimal? Price { get; set; }


    }

    public class ProgramPlayListViewModel
    {
        public long pkProgramPlayListId { get; set; }
        public string PlayList { get; set; }
        public int PlayListType { get; set; }
        public long fkProgramId { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string Description { get; set; }
        public string TrackArtist { get; set; }

        public string TrackUrl { get; set; }
        public string TrackTimeCount { get; set; }

    }

    public class ProgramMasterViewModel
    {
        public long pkProgramId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public int Session { get; set; }
        public System.DateTime CreatedDate { get; set; }

        public Nullable<bool> IsActive { get; set; }
        public bool? IsCourseOpen { get; set; }

        public DateTime ValidDate { get; set; }

        public long UserId { get; set; }

        public bool IsProgramUnlockForAllUser { get; set; }

        public string PaymentText {get;set;}

        public decimal? Price { get; set; }
    }


        public class CourseCompletinModel
        {
        public long UserId { get; set; }

        public bool IsListen { get; set; }

        public string TrackDuration { get; set; }

        public int TrackId { get; set; }

      

    }


    public class CourseCompletionCalculateModel
    {
        public long UserId { get; set; }

        public bool IsListen { get; set; }

        public string TrackDuration { get; set; }

        public long  TrackId { get; set; }

        public int ProgramId { get; set; }

        public double CompletionPercentage { get; set; }
        
    }


}