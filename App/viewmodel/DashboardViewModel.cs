﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.viewmodel
{
    public class DashboardViewModel
    {
        public int users_count { get; set; }
        public int programs_count { get; set; }

        public int CommingSoon { get; set; }
        //public int patients_count { get; set; }

        public List<ProgramviewModel> ProgramviewModelList { get; set; }

    }

    public class ProgramviewModel
    {

        public float y { get; set; }

        public string label { get; set; }
        public string indexLabel { get; set; }

    
        

    }
}