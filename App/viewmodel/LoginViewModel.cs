﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.viewmodel
{
    public class LoginViewModel
    {
        public long   pkId { get; set; }
        public string Name { get; set; }
        public string EmailId { get; set; }
        public string Password { get; set; }
        public string ImageUrl { get; set; }
        public string DeviceToken { get; set; }
        public string DeviceType { get; set; }
        public Nullable<bool> IsAdmin { get; set; }
        public string FacebookId { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime Date { get; set; }

        public bool IsSurveySubmitted { get; set; }

    }


    public class AweberViewModel
    {
        public string Name { get; set; }
        public string EmailId { get; set; }
   

    }
}