﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aweber.OAuth
{

    /// <summary>
    /// Settings required to authenticate with AWeber using OAuth v1.0
    /// Hardcoded due to the fact these settings are not user configurable
    /// </summary>
    internal class Settings
    {

        // Authorize App
        public const String authorizeApp = "https://auth.aweber.com/1.0/oauth/authorize_app/";

        // Request Token Url
        public const String requestToken = "https://auth.aweber.com/1.0/oauth/request_token";

        // Authorization Url
        public const String authorization = "https://auth.aweber.com/1.0/oauth/authorize";

        // Access Token Url
        public const String accessToken = "https://auth.aweber.com/1.0/oauth/access_token";

        // API Base
        public const String apiBase = "https://api.aweber.com/1.0/";


    }
}