﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Utility.Aweber
{

    public class IntegerStatistic : BroadcastStatistic
    {
        [JsonProperty("value")] public int Value { get; set; }
    }

    public class ListStatistic : BroadcastStatistic
    {
        [JsonProperty("value")] public IList<IDictionary<string, object>> Value { get; set; }
    }

    public class DecimalStatistic : BroadcastStatistic
    {
        [JsonProperty("value")] public decimal Value { get; set; }
    }

    public class Account
    {
        [JsonProperty("id")] public string Id { get; set; }
        [JsonProperty("http_etag")] public string HttpEtag { get; set; }
        [JsonProperty("analytics_source")] public string AnalyticsSource { get; set; }
        [JsonProperty("integrations_collection_link")] public string IntegrationsCollectionLink { get; set; }
        [JsonProperty("lists_collection_link")] public string ListsCollectionLink { get; set; }
        [JsonProperty("resource_type_link")] public string ResourceTypeLink { get; set; }
        [JsonProperty("self_link")] public string SelfLink { get; set; }
    }
    public class List
    {
        [JsonProperty("id")] public string Id { get; set; }
        [JsonProperty("http_etag")] public string HttpEtag { get; set; }
        [JsonProperty("name")] public string Name { get; set; }
        [JsonProperty("campaigns_collection_link")] public string CampaignsCollectionLink { get; set; }
        [JsonProperty("custom_fields_collection_link")] public string CustomFieldsCollectionLink { get; set; }
        [JsonProperty("draft_broadcasts_link")] public string DraftBroadcastsLink { get; set; }
        [JsonProperty("scheduled_broadcasts_link")] public string ScheduledBroadcastsLink { get; set; }
        [JsonProperty("sent_broadcasts_link")] public string SentBroadcastsLink { get; set; }
        [JsonProperty("resource_type_link")] public string ResourceTypeLink { get; set; }
        [JsonProperty("segments_collection_link")] public string SegmentsCollectionLink { get; set; }
        [JsonProperty("self_link")] public string SelfLink { get; set; }
        [JsonProperty("subscribers_collection_link")] public string SubscribersCollectionLink { get; set; }
        [JsonProperty("total_subscribed_subscribers")] public int TotalSubscribedSubscribers { get; set; }
        [JsonProperty("total_subscribers")] public int TotalSubscribers { get; set; }
        [JsonProperty("total_subscribers_subscribed_today")] public int TotalSubscribersSubscribedToday { get; set; }
        [JsonProperty("total_subscribers_subscribed_yesterday")] public int TotalSubscribersSubscribedYesterday { get; set; }
        [JsonProperty("total_unconfirmed_subscribers")] public int TotalUnconfirmedSubscribers { get; set; }
        [JsonProperty("total_unsubscribed_subscribers")] public int TotalUnsubscribedSubscribers { get; set; }
        [JsonProperty("unique_list_id")] public string UniqueListId { get; set; }
        [JsonProperty("web_form_split_tests_collection_link")] public string WebFormSplitTestsCollectionLink { get; set; }
        [JsonProperty("web_forms_collection_link")] public string WebFormsCollectionLink { get; set; }
    }

    public class Subscriber
    {
        [JsonProperty("ad_tracking")] public string AdTracking { get; set; }
        [JsonProperty("area_code")] public int? AreaCode { get; set; }
        [JsonProperty("city")] public string City { get; set; }
        [JsonProperty("country")] public string Country { get; set; }
        [JsonProperty("custom_fields")] public IDictionary<string, string> CustomFields { get; set; }
        [JsonProperty("dma_code")] public int? DmaCode { get; set; }
        [JsonProperty("email")] public string Email { get; set; }
        [JsonProperty("id")] public int Id { get; set; }
        [JsonProperty("ip_address")] public string IpAddress { get; set; }
        [JsonProperty("name")] public string Name { get; set; }
        [JsonProperty("self_link")] public string SelfLink { get; set; }
        [JsonProperty("status")] public string Status { get; set; }
    }

    public static class DictionaryExtensions
    {
        public static string ToUrlParams(this IDictionary<string, string> paramsDictionary)
        {
            return string.Join("&", paramsDictionary.Select(kvp => string.Format("{0}={1}", kvp.Key, Uri.EscapeUriString(kvp.Value))));
        }
    }

    public class UpdateSubscriber
    {
        [JsonProperty("ad_tracking")] public string AdTracking { get; set; }
        [JsonProperty("custom_fields")] public IDictionary<string, string> CustomFields { get; set; }
        [JsonProperty("email")] public string Email { get; set; }
        [JsonProperty("last_followup_message_number_sent")] public int? LastFollowupMessageNumberSent { get; set; }
        [JsonProperty("misc_notes")] public string MiscellaneousNotes { get; set; }
        [JsonProperty("name")] public string Name { get; set; }
        [JsonProperty("status")] public string Status { get; set; }
        [JsonProperty("strict_custom_fields")] public string StrictCustomFields { get; set; }
        [JsonProperty("tags")] public IDictionary<string, IList<string>> Tags { get; set; }
    }

    public class AddSubscriber
    {
        [JsonProperty("ad_tracking")] public string AdTracking { get; set; }
        [JsonProperty("custom_fields")] public IDictionary<string, string> CustomFields { get; set; }
        [JsonProperty("email")] public string Email { get; set; }
        [JsonProperty("ip_address")] public string IpAddress { get; set; }
        [JsonProperty("last_followup_message_number_sent")] public int? LastFollowupMessageNumberSent { get; set; }
        [JsonProperty("misc_notes")] public string MiscellaneousNotes { get; set; }
        [JsonProperty("name")] public string Name { get; set; }
        [JsonProperty("strict_custom_fields")] public string StrictCustomFields { get; set; }
        [JsonProperty("tags")] public IList<string> Tags { get; set; }
    }

    public class Activity
    {
        [JsonProperty("start")] public string Start { get; set; }
        [JsonProperty("total_size_link")] public string TotalSizeLink { get; set; }
        [JsonProperty("next_collection_link")] public string NextCollectionLink { get; set; }
        [JsonProperty("prev_collection_link")] public string PreviousCollectionLink { get; set; }
    }
}