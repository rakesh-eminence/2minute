﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace App.Utility.Aweber
{
    public class ApiException : Exception
    {
        public HttpStatusCode StatusCode { get; }
        public ApiError Error { get; }

        public ApiException(HttpStatusCode statusCode, ApiError error) : base(string.Format("API request failed. Error: {0}", error))
        {
            StatusCode = statusCode;
            Error = error;
        }
    }

    public class ApiError
    {

        public string Type { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
        [JsonProperty("documentation_url")]
        public string DocumentationUrl { get; set; }

        public override string ToString()
        {
            return string.Format("{0}:{1} - {2}", Status.ToString(), Type, Message);
        }
    }

    public class SimpleApiError
    {
        public string Error { get; set; }
        [JsonProperty("error_description")]
        public string Description { get; set; }

        public override string ToString()
        {
            return string.Format("{0}: {1}", Error, Description);
        }
    }
}