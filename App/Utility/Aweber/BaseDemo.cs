﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace App.Utility.Aweber
{

    public abstract class BaseDemo
    {
        protected HttpClient Client { get; }
        private readonly AuthenticationHelper _authHelper;
        private readonly JsonSerializerSettings _serializerSettings;

        // Each retry after a rate limit response will exponentially back off. This max
        // retries should be 6 or greater which will ensure that the final attempt will
        // be in the next minute and escape the rate limit window. (2⁶ = 64 seconds)
        private const int MaxRetries = 6;

        protected BaseDemo(HttpClient httpClient, AuthenticationHelper authHelper)
        {
            Client = httpClient;
            _authHelper = authHelper;
            _serializerSettings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
            _serializerSettings.Converters.Add(new BroadcastStatisticConverter());
        }

        public abstract Task ExecuteAsync(string EmailId, string Name);

        protected async Task<string> GetAccessTokenAsync()
        {
            
            var credentialsFilePath1 = HttpContext.Current.Server.MapPath("~/");
            var credentialsFilePath = HttpContext.Current.Server.MapPath("~/Credential/Awebercredentials.json");
           // var credentialsFilePath = @"D:\AWeber\credentials.json";// Path.Combine(Path.GetTempPath(), "AWeber", "credentials.json");
            return await _authHelper.RetrieveAccessTokenAsync(credentialsFilePath);
        }


        protected async Task<T> RetryAsync<T>(Task<T> getTask, int retryCount = 1)
        {
            Exception exception = null;
            foreach (var retry in Enumerable.Range(1, MaxRetries))
            {
                try
                {
                    return await getTask;
                }
                catch (ApiException ex)
                {
                    exception = ex;

                    // Only retry on a 403 (forbidden) status code with a rate limit error
                    if (ex.StatusCode != HttpStatusCode.Forbidden)
                    {
                        throw ex;
                    }

                    if (!ex.Error.Message.Contains("rate limit"))
                    {
                        throw ex;
                    }

                    //  Console.WriteResponse(ConsoleColor.Magenta, "Request was rate limited");
                    if (retry < MaxRetries)
                    {
                        // Wait longer between every attempt
                        await Task.Delay((int)Math.Pow(2, retry) * 1000);
                        //  Console.WriteResponse(ConsoleColor.Magenta, "Retry #{0}...", retry);
                    }
                }
                catch (Exception ex)
                {
                    exception = ex;
                }
            }
            //   Console.WriteResponse(ConsoleColor.Magenta, "Giving up after {0} tries", MaxRetries);
            throw exception;
        }

        protected async Task<IList<T>> GetCollectionAsync<T>(string accessToken, string url) where T : class
        {
            var entries = new List<T>();
            var hasNextLink = true;
            while (hasNextLink)
            {
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);//CreateBasicAuthHeader(clientId, clientSecret);
                    var response1 = httpClient.GetAsync(url).Result.Content.ReadAsStringAsync().Result;

                    var content = JsonConvert.DeserializeObject<CollectionResponse<T>>(response1, _serializerSettings);
                    entries.AddRange(content.Entries);
                    hasNextLink = !string.IsNullOrWhiteSpace(content.NextCollectionLink);
                    url = content.NextCollectionLink;
                }
                //var request = new HttpRequestMessage(HttpMethod.Get, url);
                //request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                //var response = await Client.SendAsync(request);
                //var contentString = await response.Content.ReadAsStringAsync();
                //if (!response.IsSuccessStatusCode)
                //{
                //    ThrowError(response.StatusCode, contentString);
                //}
                //var content = JsonConvert.DeserializeObject<CollectionResponse<T>>(contentString, _serializerSettings);
                //entries.AddRange(content.Entries);
                //hasNextLink = !string.IsNullOrWhiteSpace(content.NextCollectionLink);
                //url = content.NextCollectionLink;
            }

            return entries;
        }

        protected async Task<IList<T>> GetCollectionAsync1<T>(string accessToken, string url) where T : class
        {
            var entries = new List<T>();
            var hasNextLink = true;
            while (hasNextLink)
            {
                using (var httpClient = new HttpClient())
                {
                    try
                    {
                        httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);//CreateBasicAuthHeader(clientId, clientSecret);
                        var response1 = httpClient.GetAsync(url);
                        var result = response1.Result;
                        var res = result.Content.ReadAsStringAsync();
                        //if (!res.Status)
                        //{
                        //    ThrowError(response.StatusCode, contentString);
                        //}
                        var content1 = JsonConvert.DeserializeObject<CollectionResponse<T>>(res.Result, _serializerSettings);
                        entries.AddRange(content1.Entries);
                        hasNextLink = !string.IsNullOrWhiteSpace(content1.NextCollectionLink);
                        url = content1.NextCollectionLink;
                    }
                    catch (Exception ex)
                    {

                    }
                    //using (var content1 = new FormUrlEncodedContent())
                    //{
                    //content.Headers.Clear();
                    //content.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

                    //return JsonConvert.DeserializeObject<CollectionResponse<T>>(res,_serializerSettings);
                    // }
                }



                //var request = new HttpRequestMessage(HttpMethod.Get, url);
                //request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                //var response = await Client.SendAsync(request);
                //var contentString = await response.Content.ReadAsStringAsync();
                //if (!response.IsSuccessStatusCode)
                //{
                //    ThrowError(response.StatusCode, contentString);
                //}
                //var content = JsonConvert.DeserializeObject<CollectionResponse<T>>(contentString, _serializerSettings);
                //entries.AddRange(content.Entries);
                //hasNextLink = !string.IsNullOrWhiteSpace(content.NextCollectionLink);
                //url = content.NextCollectionLink;
            }

            return entries;
        }

        protected async Task<T> GetAsync<T>(string accessToken, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, url);
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var response = Client.SendAsync(request).GetAwaiter().GetResult();
            var contentString = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
            {
                ThrowError(response.StatusCode, contentString);
            }
            return JsonConvert.DeserializeObject<T>(contentString, _serializerSettings);
        }

        protected async Task<TResponse> UpdateAsync<TRequest, TResponse>(TRequest payload, string accessToken, string url)
        {
            var method = new HttpMethod("PATCH");
            var request = new HttpRequestMessage(method, url);
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            request.Content = new StringContent(JsonConvert.SerializeObject(payload, _serializerSettings), Encoding.UTF8);
            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var response = await Client.SendAsync(request);
            var contentString = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
            {
                ThrowError(response.StatusCode, contentString);
            }
            return JsonConvert.DeserializeObject<TResponse>(contentString);
        }

        protected async Task<Uri> CreateAsync<TRequest>(TRequest payload, string accessToken, string url)
        {

            var request = new HttpRequestMessage(HttpMethod.Post, url);
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            request.Content = new StringContent(JsonConvert.SerializeObject(payload, _serializerSettings), Encoding.UTF8);
            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var response = Client.SendAsync(request).GetAwaiter().GetResult();
            if (!response.IsSuccessStatusCode)
            {
                var contentString = await response.Content.ReadAsStringAsync();
                ThrowError(response.StatusCode, contentString);
            }
            return response.Headers.Location;
        }

        protected async Task<Uri> CreateAsync1<TRequest>(TRequest payload, string accessToken, string url)
        {
            var content = new StringContent(JsonConvert.SerializeObject(payload, _serializerSettings), Encoding.UTF8);
            content.Headers.Clear();
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            // content.Headers.Add("Content-Type", "application/json");
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);//CreateBasicAuthHeader(clientId, clientSecret);
            var response1 = await Client.PostAsync(url, content);
            if (!response1.IsSuccessStatusCode)
            {
                var contentString = await response1.Content.ReadAsStringAsync();
                ThrowError(response1.StatusCode, contentString);
            }
            //var result = response1.Result;
            // var res = result.Content.ReadAsStringAsync().Result;

            //}
            //}
            //var request = new HttpRequestMessage(HttpMethod.Post, url);
            //request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            //request.Content = new StringContent(JsonConvert.SerializeObject(payload, _serializerSettings), Encoding.UTF8);
            //request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            //var response = await Client.SendAsync(request);
            //if (!response.IsSuccessStatusCode)
            //{
            //    var contentString = await response.Content.ReadAsStringAsync();
            //    ThrowError(response.StatusCode, contentString);
            //}
            // return response.Headers.Location;
            return response1.Headers.Location;
        }

        protected async Task<Uri> CreateFormAsync(IDictionary<string, string> payload, string accessToken, string url)
        {
            try
            {


                var request = new HttpRequestMessage(HttpMethod.Post, url);
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                request.Content = new FormUrlEncodedContent(payload);
                var response = await Client.SendAsync(request);
                if (!response.IsSuccessStatusCode)
                {
                    var contentString = await response.Content.ReadAsStringAsync();
                    ThrowError(response.StatusCode, contentString);
                }
                return response.Headers.Location;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        protected async Task<T> CreateFormAsync<T>(IDictionary<string, string> payload, string accessToken, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            request.Content = new FormUrlEncodedContent(payload);
            var response = await Client.SendAsync(request);
            var contentString = await response.Content.ReadAsStringAsync();
            if (!response.IsSuccessStatusCode)
            {
                ThrowError(response.StatusCode, contentString);
            }
            return JsonConvert.DeserializeObject<T>(contentString);
        }

        protected async Task DeleteAsync(string accessToken, string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Delete, url);
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
            var response = await Client.SendAsync(request);
            if (!response.IsSuccessStatusCode)
            {
                var contentString = await response.Content.ReadAsStringAsync();
                ThrowError(response.StatusCode, contentString);
            }
        }

        private static void ThrowError(HttpStatusCode statusCode, string contentString)
        {
            //throw new ApiException(statusCode, JsonConvert.DeserializeObject<ErrorWrapper>(contentString).Error);
        }
    }
}