﻿using App.Utility.Aweber;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.IO;

namespace App.Utility
{
    public static class AweberEntry
    {
        private const string OAuthUri = "https://auth.aweber.com/oauth2";
        
        // Redirect Uri must be https and must be one of the redirects specified
        // for your integration
        private const string RedirectUri = "https://localhost";

        public static void AweberUserAdd(string EmailId, string Name)
        {

            var httpClient = new HttpClient();
            // var console = PhysicalConsole.Singleton;
            var filepath = HttpContext.Current.Server.MapPath("~/Credential/error.text");
            var authenticationHelper = new AuthenticationHelper(httpClient, OAuthUri, RedirectUri);
            var demo = new ManageSubscriberDemo(httpClient,  authenticationHelper);

            try
            {
                demo.ExecuteAsync(EmailId, Name).GetAwaiter().GetResult();
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine("successfully send" + DateTime.Now);
                }
            }
            catch (Exception e)
            {
             
                string ex = e.Message.ToString();
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(e.Message.ToString() + EmailId + DateTime.Now);
                }
            }
           
        }
    }
}