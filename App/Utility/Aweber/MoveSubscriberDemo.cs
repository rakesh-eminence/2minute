﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace App.Utility.Aweber
{

    public class MoveSubscriberDemo : BaseDemo
    {
        public MoveSubscriberDemo(HttpClient httpClient,  AuthenticationHelper authHelper) : base(httpClient, authHelper)
        {
        }

        public override async Task ExecuteAsync(string EmailId, string Name)
        {
            var accessToken = await GetAccessTokenAsync();

            // Get all the accounts entries
            const string accountsUrl = "https://api.aweber.com/1.0/accounts";
            var accounts = await GetCollectionAsync<Account>(accessToken, accountsUrl);

            // Get all the list entries for the first account
            var listsUrl = accounts.First().ListsCollectionLink;
            var lists = await GetCollectionAsync<List>(accessToken, listsUrl);

            // pick the list to move the subscriber from and to
            var originList = lists.FirstOrDefault();
            var destinationList = lists.Skip(1).FirstOrDefault();

        

            // get all the subscribers from the first list
            var subscribers = await GetCollectionAsync<Subscriber>(accessToken, originList.SubscribersCollectionLink);

            // pick the subscriber we want to move
            var subscriber = subscribers.FirstOrDefault();

          

            var moveParams = new Dictionary<string, string>
            {
                {"ws.op", "move"},
                {"list_link", destinationList.SelfLink}
            };
           
        }
    }
}