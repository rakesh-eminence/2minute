﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Utility
{

    public class AuthorizationUrl
    {
        public string Url { get; set; }
        public string State { get; set; }
    }
}