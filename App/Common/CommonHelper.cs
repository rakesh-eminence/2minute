﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace App.Common
{
    public class CommonHelper : ApiController
    {
        public static string GetConnectionString()
        {
            // Put the name the Sqlconnection from WebConfig..
            return ConfigurationManager.ConnectionStrings["BrainApp"].ConnectionString;
        }
        public static void CreateDirectories(string _dir)
        {
            if (!Directory.Exists(_dir))
            {
                Directory.CreateDirectory(_dir);
            }
        }

        public static void DeleteDirectory(string _dir, bool Subdirectory)
        {
            if (Directory.Exists(_dir))
            {
                Directory.Delete(_dir, Subdirectory);
            }
        }

        public static void DeleteFiles(string _dir)
        {
            if (Directory.Exists(_dir))
            {
                Array.ForEach(Directory.GetFiles(_dir), File.Delete);
            }
        }

        public HttpResponseMessage APIResponse(HttpStatusCode statusCode, bool status, object Response, object Data, HttpRequestMessage Requests)
        {

            var result = new
            {
                Status = status,
                Message = Response,
                data = Data

            };

            return Requests.CreateResponse(statusCode, result);
        }

    }

    public static class CommonMessage
    {
        public static string NotAuthorised = "Invalid user name or password.";
        public static string FailedSignUP = "Oops ! Signup failed.";
        public static string FailedUpdatePassword = "Your old password do not match!";
        public static string ExceptionMsg = "Oops ! Something wrong. Try after sometimes.";
        public static string BadRequest = "Bad Request";
        public static string SuccessLogin = "Welcome to 2 Min Brain APP.";

        public static string SuccessSignUp = "SignUp Successfully.";
        public static string SuccessPWDChanged = "Your password has been successfully changed.";
        public static string FailedEmailVerification = "Kindly enter your registered emailId";
        public static string OTP_PasswordReset = "OTP has been sent to you. Please enter OTP to Login";
        public static string PasswordReset = "Password has been sent to you";
        public static string Subject_PasswordReset = "Password Reset.";
        public static string FailedPWDVerification = "Entered password is not valid.";
        public static string UnlockCourse = "Course has been unlocked successfully.";
        public static string EmailExists = "Email Id Already Exists";
        public static string Success = "Success";
        public static string Error = "";
        public static string PlayListDuplicate = "Play list already exist ";
        public static string PlayListCreateSuccessfully = "Play List Created Successfully ";
        public static string PlayListRemoveSuccessfully = "Play List Remove successfully";
        public static string dataformat = "Please check the data";
        public static string datanotExist = "Data not Exist";
        public static string trackAddedsuccessfully = "Track Added in playlist successfully";
        public static string BasicTrackRelease = "Today's 2 Minute Brainflowness Exercise is Ready for You!";
        public static string BonusTrackRelease = "Surprise! a new bonus track is ready for you.";
        public static string ContactSave = "Message sent.";


        //Generate RandomNo
        public static int GenerateRandomNo()
        {
            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max);
        }

    }

}