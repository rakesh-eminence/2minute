﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Web;

namespace App.Common
{
    public static class EmailHelper
    {
        public static bool SendEmail(String ToEmail, string cc, string bcc, String Subj, string Message)
        {
            SmtpSection secObj = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
            using (MailMessage mm = new MailMessage())
            {
                mm.From = new MailAddress(secObj.From);
                mm.Subject = Subj;
                mm.Body = Message;
                mm.IsBodyHtml = true;


                string[] ToMuliId = ToEmail.Split(',');
                foreach (string ToEMailId in ToMuliId)
                {
                    mm.To.Add(new MailAddress(ToEMailId)); //adding multiple TO Email Id
                }

                if (!string.IsNullOrEmpty(cc))
                {
                    string[] CCId = cc.Split(',');

                    foreach (string CCEmail in CCId)
                    {
                        mm.CC.Add(new MailAddress(CCEmail)); //Adding Multiple CC email Id
                    }
                }

                if (!string.IsNullOrEmpty(cc))
                {
                    string[] bccid = bcc.Split(',');

                    foreach (string bccEmailId in bccid)
                    {
                        mm.Bcc.Add(new MailAddress(bccEmailId)); //Adding Multiple BCC email Id
                    }
                }

                SmtpClient smtp = new SmtpClient();
                smtp.Host = secObj.Network.Host;
                smtp.EnableSsl = secObj.Network.EnableSsl;
                NetworkCredential NetworkCred = new NetworkCredential(secObj.Network.UserName, secObj.Network.Password);

                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;

                try
                {
                    smtp.Send(mm);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }

            }
        }

        //public static bool SendEmail(String ToEmail, string cc, string bcc, String Subj, string Message)
        //{
        //    SmtpSection secObj = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
        //    using (MailMessage mm = new MailMessage())
        //    {
        //        mm.From = new MailAddress(secObj.From);
        //        mm.Subject = Subj;
        //        mm.Body = Message;
        //        mm.IsBodyHtml = true;


        //        string[] ToMuliId = ToEmail.Split(',');
        //        foreach (string ToEMailId in ToMuliId)
        //        {
        //            mm.To.Add(new MailAddress(ToEMailId)); //adding multiple TO Email Id
        //        }

        //        if (!string.IsNullOrEmpty(cc))
        //        {
        //            string[] CCId = cc.Split(',');

        //            foreach (string CCEmail in CCId)
        //            {
        //                mm.CC.Add(new MailAddress(CCEmail)); //Adding Multiple CC email Id
        //            }
        //        }

        //        if (!string.IsNullOrEmpty(cc))
        //        {
        //            string[] bccid = bcc.Split(',');

        //            foreach (string bccEmailId in bccid)
        //            {
        //                mm.Bcc.Add(new MailAddress(bccEmailId)); //Adding Multiple BCC email Id
        //            }
        //        }



        //        MailMessage mail = new MailMessage();
        //        SmtpClient SmtpServer = new SmtpClient();
        //        mail.From = new MailAddress("al@2minutebrain.com");
        //        SmtpServer.Host = "smtpserver";
        //        SmtpServer.Port = 25;
        //        SmtpServer.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
        //        try
        //        {
        //            SmtpServer.Send(mm);
        //            return true;
        //        }
        //        catch (Exception ex)
        //        {
        //            Debug.WriteLine("Exception Message: " + ex.Message);
        //            if (ex.InnerException != null)
        //                Debug.WriteLine("Exception Inner:   " + ex.InnerException);
        //            return false;
        //        }
        //    }
        //}
    }
}