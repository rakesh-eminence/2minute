﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace MeditationService
{
    public partial class Service1 : ServiceBase
    {
        Timer timer = new Timer(); // name space(using System.Timers;)
        public Service1()
        {
            InitializeComponent();
        }

  
        protected override void OnStart(string[] args)
        {
            WriteToFile("Service is started at " + DateTime.Now);
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Interval = (1000 * 60)*60; //number in milisecinds  
            timer.Enabled = true;
            


            try
            {

                using (var client = new HttpClient())
                {

                    
                    var webUrl = "http://23.111.138.246:8029/Api/";
                    var uri = "Login/SendNotificationopentrack";
                    client.BaseAddress = new Uri(webUrl);
                    client.DefaultRequestHeaders.ConnectionClose = true;
                    //Set Basic Auth
                    var user = "Eminence";
                    var password = "Eminence!Tech!";
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    var result = client.GetAsync(uri).Result;
        
                }
            }
            catch (Exception ex)
            {

                WriteToFile(ex.Message + DateTime.Now);

            }



        }
        protected override void OnStop()
        {
            WriteToFile("Service is stopped at " + DateTime.Now);
        }
        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
         
            try
            {

                using (var client = new HttpClient())
                {
                    
                    var webUrl = "http://23.111.138.246:8029/Api/";
                    var uri = "Login/SendNotificationopentrack";
                    client.BaseAddress = new Uri(webUrl);
                    client.DefaultRequestHeaders.ConnectionClose = true;
                    //Set Basic Auth
                    var user = "Eminence";
                    var password = "Eminence!Tech!";
                    var base64String = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{user}:{password}"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", base64String);
                    var result = client.GetAsync(uri).Result;

                }
                WriteToFile("Working Fine service" + DateTime.Now);


            }
            catch (Exception ex)
            {

                WriteToFile(ex.Message + DateTime.Now);

            }

        }
        public void WriteToFile(string Message)
        {


            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            //string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceLog_" + "xservice" + ".txt";
            if (!File.Exists(filepath))
            {
                // Create a file to write to.   
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }
    }
}
